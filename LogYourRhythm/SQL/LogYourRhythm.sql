DROP DATABASE IF EXISTS LogYourRhythm;

CREATE DATABASE LogYourRhythm;

USE LogYourRhythm;


CREATE TABLE LoggedUser(
	username VARCHAR(30) primary key,
    password VARCHAR(100) not null,
    firstName VARCHAR(20) default "",
    lastName VARCHAR(20) default "",
    goal TEXT default "",
    height INTEGER default 0,
    weight INTEGER default 0
);

CREATE TABLE LoggedUserImage(
	username VARCHAR(30) not null,
    foreign key fk1(username) references LoggedUser(username)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	image LONGBLOB not null,
    timeUploaded TIMESTAMP not null
		ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY(username,timeUploaded)
);

CREATE TABLE UserFriends(
	username1 VARCHAR(30) not null,
    username2 VARCHAR(30) not null,
	PRIMARY KEY(username1,username2),
    foreign key fk1(username1) references LoggedUser(username)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
    foreign key fk2(username2) references LoggedUser(username)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE Workout(
	workoutID INTEGER primary key auto_increment,
    whenCreated TIMESTAMP not null
		ON UPDATE CURRENT_TIMESTAMP,
    workoutName VARCHAR(20) not null,
	workoutFrequency INTEGER not null default 0
);

CREATE TABLE Activity(
	activityID INTEGER primary key auto_increment,
    activityName VARCHAR(30) not null,
    units VARCHAR(20) not null,
    whatMeasured VARCHAR(20) not null,
	goal INTEGER not null default 0
);

CREATE TABLE User_Workout(
	username VARCHAR(30) not null,
    workoutID INTEGER not null,
    PRIMARY KEY (username,workoutID),
    foreign key fk1(username) references LoggedUser(username)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
    foreign key fk2(workoutID) references Workout(workoutID)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE User_Activity(
	username VARCHAR(30) not null,
    activityID INTEGER not null,
	progressBoolean INTEGER not null default 0,
    PRIMARY KEY(username,activityID,progressBoolean),
    foreign key fk1(username) references LoggedUser(username)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
    foreign key fk2(activityID) references Activity(activityID)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE Workout_Activity(
	workoutID INTEGER not null,
    activityID INTEGER not null,
	progress INTEGER not null default 0,
	PRIMARY KEY(workoutID,activityID,progress),
    foreign key fk1(workoutID) references Workout(workoutID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
    foreign key fk2(activityID) references Activity(ActivityID)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);


CREATE TABLE Activity_Data(
	workoutID INTEGER not null,
	activityID INTEGER not null,
	timeRecorded TIMESTAMP not null
		ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY(workoutID,activityID,timeRecorded),
	FOREIGN KEY fk1(workoutID) references Workout(workoutID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY fk2(activityID) references Activity(activityID)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	activityData INTEGER not null
);
