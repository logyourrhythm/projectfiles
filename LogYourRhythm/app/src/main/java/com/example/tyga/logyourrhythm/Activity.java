package com.example.tyga.logyourrhythm;

import java.io.Serializable;
import java.sql.Time;
import java.util.HashMap;

/*
Added ID member variable so that if there are two identical activities in the database belonging to two different users,
    they can be distinguished
*/

public class Activity implements Serializable {

    private static final long serialVersionUID = 1;
    private String name;
    private String units;
    private String measurements;
    private Integer goal;
    protected Integer ID;
    private String workoutName;
    private HashMap<Long, Integer> dataOverTime; //each activity has a history of its previous workouts
    private Boolean progressIsTracked;
    //used to record and track progress
    //used hashmap instead of hashtable to preserve order

    public Activity(String name, String units, String measurements) {
        this.name = name;
        this.units = units;
        this.measurements = measurements;
        dataOverTime = new HashMap<Long, Integer>();
        goal = null;
        progressIsTracked = false;
    }

    public String getName() {
        return name;
    }

    public String getUnits() {
        return units;
    }

    public String getMeasurements() {
        return measurements;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public Integer getGoal() {
        if (goal == null) {
            return null;
        }
        return goal;
}

    public void setWorkoutName(String workoutName) {this.workoutName = workoutName;}

    public String getWorkoutName() {return workoutName;}

    public void addData(Long time, Integer value) { //time in seconds
        dataOverTime.put(time, value);
    }

    public HashMap<Long, Integer> getData() {
        return dataOverTime;
    }

    public boolean isProgressTracked() {return progressIsTracked;}

    public void setProgressIsTracked(Boolean tracked) {progressIsTracked = tracked;}

}