package com.example.tyga.logyourrhythm;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tiffany on 4/16/2016.
 */
public class ActivityListAdapter extends ArrayAdapter<Activity> {
    private ArrayList<Activity> activities;

    public ActivityListAdapter(Context context, int textViewResourceId, ArrayList<Activity> activities) {
        super(context, textViewResourceId, activities);
        this.activities = activities;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.custom_list_view, parent, false);
        }
        Activity activity = activities.get(position);
        if (activity != null) {
            TextView activityName = (TextView) v.findViewById(R.id.activityname);
            TextView activityInfo = (TextView) v.findViewById(R.id.activityinfo);
            ImageView activityIcon = (ImageView) v.findViewById(R.id.activityicon);
            if (activity != null) {
                activityName.setText(activity.getName());
            }
            if(activityInfo != null) {
                String text = activity.getMeasurements();
                if (!text.equals("Repetitions")) {
                    text = text + " in " + activity.getUnits();
                }
                activityInfo.setText(text);
            }
            //Setting icon for each activity
            Drawable drawable = null;
            if (activity.getMeasurements().equals("Distance")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.dist);
            } else if (activity.getMeasurements().equals("Weight")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.weight);
            } else if (activity.getMeasurements().equals("Time")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.time);
            } else if (activity.getMeasurements().equals("Repetitions")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.reps);
            }
            if (drawable != null) activityIcon.setImageDrawable( drawable );
        }
        return v;
    }
}