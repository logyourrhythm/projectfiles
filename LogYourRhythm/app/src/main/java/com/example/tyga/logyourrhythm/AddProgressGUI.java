package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Vector;

public class AddProgressGUI extends AppCompatActivity {
    TabHost tabHost;
    private LoggedUser user;
    private Vector<Workout> userWorkouts;
    private HashMap<Workout, Vector<Activity>> workoutActivitiesTracked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_progress_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //To set up tabs, get the user's workouts
        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser) bundle.getSerializable("LoggedUser");
        userWorkouts = user.getWorkoutManager().getUserWorkouts();
        workoutActivitiesTracked = user.getProgressManager().getWorkoutToActivities();

        //Create a new tab for each of the user's workouts
        tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        for (Workout workout : userWorkouts) {
            final Workout w = workout;
            TabHost.TabSpec tab = tabHost.newTabSpec(w.getWorkoutName());
            tab.setIndicator(w.getWorkoutName()); //set the tab name
            tab.setContent(R.id.listView); //get the listview that will be the contents of the tab
            tab.setContent(new TabHost.TabContentFactory() {
                @Override
                public View createTabContent(String tag) {
                    final Vector<String> workoutActivitesList = new Vector<String>();
                    final Vector<Activity> availableActivties = new Vector<Activity>();
                    if (workoutActivitiesTracked.containsKey(w)) {
                        Vector<Activity> activitesAlreadyTracked = workoutActivitiesTracked.get(w);
                        for (Activity a : w.getActivitiesList()) {
                            //dont want to display any activities that are already being tracked
                            if (!activitesAlreadyTracked.contains(a)) {
                                workoutActivitesList.add(a.getName());
                                availableActivties.add(a);
                            }
                        }
                    } else {
                        for (Activity a : w.getActivitiesList()) {
                            workoutActivitesList.add(a.getName());
                            availableActivties.add(a);
                        }
                    }
                    ListView lv = new ListView(AddProgressGUI.this);
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddProgressGUI.this,
                                    android.R.layout.simple_list_item_1, workoutActivitesList);
                    //set the listview adapter
                    lv.setAdapter(arrayAdapter);
                    //be sure to set on itme click listener
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                            //First double check that the user wants to add this activity
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddProgressGUI.this);
                            builder.setMessage("Track progress for " + workoutActivitesList.elementAt(position) + "?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //add progress to progress manager
                                    ProgressManager pm = user.getProgressManager();
                                    availableActivties.elementAt(position).setProgressIsTracked(true);
                                    pm.addProgressItem(availableActivties.elementAt(position), w);

                                    //update to databse that user wants to track certain activity for cerain workout
                                    (new UpdateActivityProgressStatusToServer(user,w.getWorkoutName(),availableActivties.elementAt(position).getName())).execute();

                                    //update user
                                    user.setProgressManager(pm);
                                    //send activity back to the progress menu, there start up the progress item gui
                                    Intent intent = new Intent(AddProgressGUI.this, ProgressMenuGUI.class);
                                    intent.putExtra("ProgressActivity", availableActivties.elementAt(position));
                                    intent.putExtra("LoggedUser", user);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.setFlags(intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    setResult(RESULT_OK, intent);
                                    AddProgressGUI.this.finish();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });
                    return lv;
                }
            });
            tabHost.addTab(tab);
        }
    }
}
