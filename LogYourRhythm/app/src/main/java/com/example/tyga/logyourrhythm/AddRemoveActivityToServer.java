package com.example.tyga.logyourrhythm;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/10/2016.
 */
public class AddRemoveActivityToServer extends AsyncTask<Void,Void,Void> {
    private String activityName, whatMeasured, units, addOrRemoveSpec;
    private LoggedUser user;
    private Integer goal;
    public AddRemoveActivityToServer(LoggedUser user, String activityName, String whatMeasured, String units, Integer goal, String addOrRemove){
        this.activityName = activityName;
        this.whatMeasured = whatMeasured;
        this.units = units;
        this.user = user;
        addOrRemoveSpec = addOrRemove;
    }

    @Override
    protected Void doInBackground(Void ... params){
        if(addOrRemoveSpec.equals("add")) addActivity();
        else removeActivity();
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        //go back to the edit workout GUI
    }

    private void removeActivity(){
        PreparedStatement ps = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("DELETE FROM Activity WHERE activityID = " +
                    "(SELECT activityID FROM User_Activity,Activity WHERE activityName =? and units =? and whatMeasured=?" +
                    " and User_Activity.activityID = Activity.activityID))");
            ps.setString(1, activityName);
            ps.setString(2,units);
            ps.setString(3, whatMeasured);
            ps.executeUpdate();
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
    }

    private void addActivity(){
        PreparedStatement ps = null,ps1=null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("INSERT INTO Activity(activityName,units,whatMeasured) VALUES(?,?,?)");
            ps.setString(1, activityName);
            ps.setString(2,units);
            ps.setString(3, whatMeasured);
            ps.execute();
//            Log.d("Username is", user.getUsername());
            ps1 = conn.prepareStatement("INSERT INTO User_Activity(username,activityID) VALUES(?," +
                    "(SELECT activityID FROM Activity ORDER BY activityID DESC LIMIT 1))");
            ps1.setString(1,user.getUsername());
            ps1.execute();
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(ps1!=null) ps1.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
    }
}
