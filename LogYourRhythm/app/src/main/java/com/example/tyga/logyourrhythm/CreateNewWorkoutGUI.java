package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateNewWorkoutGUI extends AppCompatActivity {

    //COMPONENTS AND DATA MEMBERS
    Button createWorkoutButton;
    EditText workoutNameTextEdit;
    private String WorkoutName;
    private LoggedUser user;

    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_workout_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Get user object
        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser ) bundle.get("LoggedUser");

        //need to make this take the user back to the main my workouts menu
        //this floating action button corresponds to a cancel action
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //getting other components
        workoutNameTextEdit = (EditText) findViewById(R.id.workout_name_text);
        createWorkoutButton = (Button) findViewById(R.id.create_workout_button);
    }

    //METHODS AND ACTIONS
    public void createNewWorkoutAction(View v) {
        String newWorkoutName = workoutNameTextEdit.getText().toString();
        if (newWorkoutName.isEmpty()) {
            //check that the workout name is valid
            //also check the database for duplicated?
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateNewWorkoutGUI.this);
            builder.setMessage("Workout Name is Invalid")
                    .setTitle("Error");
            // Add the buttons
            builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            //check that the user doesn't already have a workout of that name
            boolean isValidName = true;
            for (Workout w: user.getWorkoutManager().getUserWorkouts()) {
                if (w.getWorkoutName().equals(newWorkoutName)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateNewWorkoutGUI.this);
                    builder.setMessage("Workout with this name already exists!").setTitle("Error");
                    builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    isValidName = false;
                    break;
                }
            }
            if (isValidName) {
                //workout name okay, move on to next screen to add activities
                Intent intent = new Intent(CreateNewWorkoutGUI.this,CreatingNewWorkoutGUI.class);
                intent.putExtra("LoggedUser", user);
                intent.putExtra("WorkoutName", workoutNameTextEdit.getText().toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                startActivity(intent);
                finish();
            }
        }
    }



}
