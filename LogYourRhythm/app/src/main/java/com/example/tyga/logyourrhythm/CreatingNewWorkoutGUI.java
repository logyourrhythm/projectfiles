package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreatingNewWorkoutGUI extends AppCompatActivity {
    //DATA MEMBERS
    Button addActivityButton;
    Button finishButton;
    private LoggedUser user;
    private String workoutName;
    private ListView activitiesListView;
    private ArrayList<Activity> activitites;
    private Workout newWorkout;
    private ListView l;
    private ActivityListAdapter arrayAdapter;


    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_new_workout_gui);

        //get user and workout name information from previous screen
        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser ) bundle.get("LoggedUser");
        workoutName = (String) bundle.get("WorkoutName");

        newWorkout = new Workout();
        newWorkout.setWorkoutName(workoutName);

        //LISTVIEW of activity names
        activitiesListView = (ListView) findViewById(R.id.activitiesListView);
        activitites = new ArrayList<Activity>();
        arrayAdapter = new ActivityListAdapter(this, R.layout.custom_list_view, activitites);
        activitiesListView.setAdapter(arrayAdapter);

        //TEXTVIEW set the title of the screen to the name of the user's new workout
        TextView workoutNameTextView = (TextView) findViewById(R.id.workoutNameTextView);
        workoutNameTextView.setText(workoutName);

        //BUTTONS
        addActivityButton = (Button) findViewById(R.id.addActivityButton);
        finishButton = (Button) findViewById(R.id.finishButton);
        addActivityButton.setVisibility(View.VISIBLE);
        finishButton.setVisibility(View.VISIBLE);
    }

    //METHODS AND ACTIONS
    //when user selects this button, the completed workout will be created and the user will
    //be taken back to their My Workouts page. Their New workout will be displayed at the bottom
    public void finishButtonAction(View view) {
    //check that the user isn't creating an empty workout
        if (newWorkout.getActivitiesList().size() == 0) {
            //alert user that workout has no activities
            AlertDialog.Builder builder = new AlertDialog.Builder(CreatingNewWorkoutGUI.this);
            builder.setMessage("Please add activities to your workout!")
                    .setTitle("Error");
            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            //add the workout to the user's workout manager
            user.getWorkoutManager().addWorkout(newWorkout);
            //add this workout to the database
            (new AddWorkoutToServer(newWorkout,newWorkout.getActivitiesList(),user)).execute();
            Intent i = new Intent(CreatingNewWorkoutGUI.this, MyWorkoutsGUI.class);
            i.putExtra("NewWorkoutName", workoutName);
            i.putExtra("LoggedUser", user);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(i.FLAG_ACTIVITY_SINGLE_TOP);
            setResult(RESULT_OK, i);
            finish();
        }

    }

    //this method will allow the user to create a new activity
    public void addActivityButtonAction(View view) {
        Intent intent = new Intent(CreatingNewWorkoutGUI.this,activityGUI.class);
        intent.putExtra("LoggedUser", user);
        intent.putExtra("WorkoutName", newWorkout.getWorkoutName());
        startActivityForResult(intent, RequestCodes.creatingWorkoutCode);
    }

    //After the user creates a new activity and sends it back to this window
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodes.creatingWorkoutCode && resultCode == RESULT_OK && data != null) {
            Activity activity = (Activity) data.getSerializableExtra("Activity");
            user = (LoggedUser) data.getSerializableExtra("LoggedUser");
            //add new activity to the ongoing new workout object
            newWorkout.addActivity(activity);
            //add new activity to user
            user.getActivityManager().addActivity(activity);
            //also add this new activity to the database
            (new AddRemoveActivityToServer(user,activity.getName(),activity.getMeasurements(), activity.getUnits(),0,"add")).execute();
            //now add the activity to the gui
            activitites.add(activity);
            arrayAdapter.notifyDataSetChanged();
            Toast.makeText(CreatingNewWorkoutGUI.this, activity.getName() + " was added",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (newWorkout.getActivitiesList().size() == 0) {
            //alert user that workout has no activities
            AlertDialog.Builder builder = new AlertDialog.Builder(CreatingNewWorkoutGUI.this);
            builder.setMessage("Please add activities to your workout!")
                    .setTitle("Error");
            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            //check that the user wants to create this activity
            AlertDialog.Builder builder = new AlertDialog.Builder(CreatingNewWorkoutGUI.this);
            builder.setMessage("Finished adding activities to your workout?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    //add the workout to the user's workout manager
                    user.getWorkoutManager().addWorkout(newWorkout);
                    //add this workout to the database
                    (new AddWorkoutToServer(newWorkout,newWorkout.getActivitiesList(),user)).execute();
                    Intent i = new Intent(CreatingNewWorkoutGUI.this, MyWorkoutsGUI.class);
                    i.putExtra("NewWorkoutName", workoutName);
                    i.putExtra("LoggedUser", user);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(i.FLAG_ACTIVITY_SINGLE_TOP);
                    setResult(RESULT_OK, i);
                    finish();
                    CreatingNewWorkoutGUI.super.onBackPressed();
                }
            });
            builder.setNegativeButton("Continue adding activities", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


}
