package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class CurrentWorkoutGUI extends AppCompatActivity {

    private LoggedUser loggedUser;
    private GuestUser guestUser;
    private String userType;
    private Workout workout;
    private Button finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_workout_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        finishButton = (Button)findViewById(R.id.finishButton);

        Bundle bundle = getIntent().getExtras();
        userType = (String)bundle.getSerializable("UserType");
        if(userType.equals("loggedUser")) {
            loggedUser = (LoggedUser)bundle.getSerializable("LoggedUser");
            guestUser = null;
        }
        else if(userType.equals("guestUser")) {
            guestUser = (GuestUser)bundle.getSerializable("GuestUser");
            loggedUser = null;
        }

        workout = (Workout)bundle.getSerializable("Workout");

        //get all the Activity Names to generate list
        ArrayList<String> list = new ArrayList<String>();
        for(Activity activity : workout.getActivitiesList()) {
            list.add(activity.getName()+" ("+activity.getUnits()+")");
        }

        MyWorkoutAdapter adapter = new MyWorkoutAdapter(list, this);
        final ListView activityList = (ListView)findViewById(R.id.currentWorkoutList);
        activityList.setAdapter(adapter);

        finishButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(userType.equals("loggedUser")) {
                    Vector<Activity> activities = workout.getActivitiesList();
                    HashMap<Activity,Integer> activityAndInput = new HashMap<Activity,Integer>();
                    View view;
                    EditText et;
                    int i = 0;
                    for(Activity activity : activities) { //go through the activity list, extract the user's input and store it in a map with the activity as the key
                        view = activityList.getChildAt(i);
                        et = (EditText) view.findViewById(R.id.workout_list_item);
                        activityAndInput.put(activity, Integer.parseInt(et.getText().toString()));
                        i++;
                    }
                    Long timeInSeconds = (System.currentTimeMillis());
                    Iterator it = activityAndInput.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry)it.next();
                        Activity activity = (Activity)pair.getKey();
                        Activity localActivity = loggedUser.getActivityManager().getActivityByName(activity.getName()); //get the specified activity in the user class
                        localActivity.addData(timeInSeconds, (Integer)pair.getValue());
                    }

                    loggedUser.getWorkoutManager().increaseWorkoutFrequency(workout);
                    new ActivityDataToServer(loggedUser,activityAndInput,workout.getWorkoutName()).execute();
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("LoggedUser", loggedUser);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }

                else {
                    finish();
                }
            }
        });
    }

    class ActivityDataToServer extends AsyncTask<Void,Void,Void> {

        private LoggedUser user;
        private String workoutName;
        private HashMap<Activity,Integer> activityData;

        public ActivityDataToServer(LoggedUser user, HashMap<Activity, Integer> activityData, String workoutName){
            this.user = user;
            this.activityData = activityData;
            this.workoutName = workoutName;
        }

        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null, ps1= null,ps2=null;
            ResultSet workoutID;
            Connection conn = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                for(Map.Entry<Activity,Integer> data: activityData.entrySet()){
                    ps = conn.prepareStatement("INSERT INTO Activity_Data(workoutID,activityID,timeRecorded,activityData)" +
                            "VALUES((SELECT User_Workout.workoutID FROM User_Workout, Workout WHERE Workout.workoutName= " +
                            "? and User_Workout.username=? and User_Workout.workoutID=Workout.workoutID)," +
                            "(SELECT Workout_Activity.activityID FROM Activity, Workout_Activity, User_Activity, Workout WHERE " +
                            "User_Activity.username = ? and Workout.workoutName = ? and Workout_Activity.workoutID = " +
                            "Workout.workoutID and Activity.activityID = Workout_Activity.activityID " +
                            "and User_Activity.activityID = " +
                            "Workout_Activity.activityID and activityName = ?),?,?)");
                    ps.setString(1, workoutName);
                    ps.setString(2, user.getUsername());
                    ps.setString(3, user.getUsername());
                    ps.setString(4, workoutName);
                    ps.setString(5, data.getKey().getName());
                    ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                    ps.setInt(7,data.getValue());
                    ps.execute();
                }
                ps1 = conn.prepareStatement("SELECT User_Workout.workoutID FROM User_Workout,Workout WHERE" +
                        " User_Workout.workoutID = Workout.workoutID AND username = ? AND" +
                        " workoutName = ?");
                ps1.setString(1,user.getUsername());
                ps1.setString(2,workoutName);
                workoutID = ps1.executeQuery();
                if(workoutID.next()){
                    ps2 = conn.prepareStatement("UPDATE User_Workout SET workoutFrequency =" +
                            "workoutFrequency+1 WHERE workoutID = ?");
                    ps2.setInt(1, workoutID.getInt("workoutID"));
                    ps2.executeUpdate();
                }
//            INSERT INTO Activity_Data(workoutID,activityID,timeRecorded,activityData)
//            VALUES((SELECT User_Workout.workoutID FROM User_Workout, Workout WHERE Workout.workoutName=
//                    'Legs' and User_Workout.username='huy' and User_Workout.workoutID=Workout.workoutID),
//                    (SELECT Workout_Activity.activityID FROM Activity, Workout_Activity, User_Activity, Workout WHERE
//                    User_Activity.username = 'huy' and Workout.workoutName = 'Legs' and Workout_Activity.workoutID =
//                    Workout.workoutID and Activity.activityID = Workout_Activity.activityID
//                    and User_Activity.activityID =
//                    Workout_Activity.activityID and activityName = 'Low Bar'),NOW(),225);
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(ps1!=null) ps1.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }

        protected void onPostExecute(Void result){
            //not sure what to put here yet
        }
    }


}
