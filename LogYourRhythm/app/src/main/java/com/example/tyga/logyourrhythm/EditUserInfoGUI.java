package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

public class EditUserInfoGUI extends AppCompatActivity {
    private TextView firstNameEditText, lastNameEditText, heightEditText, weightEditText, goalEditText;
    private ImageView profilePictureImageView;
    private LoggedUser user;
    private String firstName,lastName,goal;
    private byte[] profilePictureAsBytes;
    private int height, weight;
    private Button submitButton;
    private static final int PICK_IMAGE=1;
    private boolean profilePictureChosen = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_info_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser) bundle.getSerializable("LoggedUser");
        profilePictureAsBytes = bundle.getByteArray("profilePicture");

        profilePictureImageView = (ImageView) findViewById(R.id.no_profile_picture);
        profilePictureImageView.setImageBitmap(BitmapFactory.decodeByteArray(profilePictureAsBytes,0,profilePictureAsBytes.length));
        firstNameEditText = (TextView) findViewById(R.id.firstNameEditText);
        lastNameEditText = (TextView) findViewById(R.id.lastNameEditText);
        heightEditText = (TextView) findViewById(R.id.heightEditText);
        weightEditText = (TextView) findViewById(R.id.weightEditText);
        goalEditText = (TextView) findViewById(R.id.goalEditText);
        submitButton = (Button) findViewById(R.id.submitButton);

        firstNameEditText.setText(user.getFirstName());
        lastNameEditText.setText(user.getLastName());
        heightEditText.setText(String.valueOf(user.getHeight()));
        weightEditText.setText(String.valueOf(user.getWeight()));
        goalEditText.setText(user.getGoal());
    }

    public void submitUpdatedInfo(View v) throws InterruptedException, ExecutionException{

        firstName = firstNameEditText.getText().toString();
        lastName = lastNameEditText.getText().toString();
        goal = goalEditText.getText().toString();
        height = Integer.parseInt(heightEditText.getText().toString());
        weight = Integer.parseInt(weightEditText.getText().toString());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGoal(goal);
        user.setHeight(Integer.toString(height));
        user.setWeight(Integer.toString(weight));
        (new UpdatedUserInfoToServer()).execute();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("ProfilePicture", profilePictureAsBytes);
        resultIntent.putExtra("LoggedUser", user);
        setResult(RESULT_OK, resultIntent);
        //then give back the updated user information for UserProfileGUI to populate
        finish();
    }

    public void chooseProfilePicture(View v){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select Picture"), PICK_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null!=data) {
            decodeUri(data.getData());
            profilePictureChosen = true;



//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize=4;
//
//            String[] filePathColumn = { MediaStore.Images.Media.DATA };
//            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//            String picturePath = cursor.getString(columnIndex);
//            cursor.close();
//            profilePictureImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath,options));

//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
//                profilePictureImageView = (ImageView) findViewById(R.id.no_profile_picture);
//                profilePictureImageView.setImageBitmap(bitmap);
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
//                profilePictureAsBytes = bos.toByteArray();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

    public void decodeUri(Uri uri) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to
            final int REQUIRED_SIZE = 2048;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(imageSource, null, o2);
            profilePictureImageView.setImageBitmap(bitmap);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            profilePictureAsBytes = bos.toByteArray();

        } catch (FileNotFoundException e) {
            // handle errors
        }finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
    }

    class UpdatedUserInfoToServer extends AsyncTask<Void,Void,Void>{
        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null, ps1=null;
            Connection conn = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("UPDATE LoggedUser SET firstName=?, lastName=?," +
                        " goal=?, height=?, weight=? WHERE username=?");
                ps.setString(1,firstName);
                ps.setString(2,lastName);
                ps.setString(3,goal);
                ps.setInt(4, height);
                ps.setInt(5, weight);
                ps.setString(6, user.getUsername());
                ps.executeUpdate();
                if(profilePictureChosen) {
                    ps1 = conn.prepareStatement("UPDATE LoggedUserImage SET image=? WHERE username=?");
                    ps1.setBinaryStream(1, new ByteArrayInputStream(profilePictureAsBytes), profilePictureAsBytes.length); //attempt to get length of null array
                    ps1.setString(2, user.getUsername());
                    ps1.executeUpdate();
                }
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(ps1!=null) ps1.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }
    }
}
