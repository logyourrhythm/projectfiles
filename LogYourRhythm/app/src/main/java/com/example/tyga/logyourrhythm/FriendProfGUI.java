package com.example.tyga.logyourrhythm;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

public class FriendProfGUI extends AppCompatActivity {


    private TextView firstNameEditText, lastNameEditText, heightEditText, weightEditText, goalEditText, mostFrequentTextView;
    private ImageView profilePictureImageView;
    private LoggedUser user;
    private String firstName, lastName, goal,mostFrequentWorkoutname;
    private int height, weight;
    private byte[] profilePictureAsBytes;
    private Bitmap profilePictureAsBitmap;
    private String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_prof_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser)bundle.getSerializable("LoggedUser");
        username = bundle.getString("FriendProfile");

        try{
            (new UserInfoFromServer()).execute().get();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }catch(ExecutionException ee){
            ee.printStackTrace();
        }
        profilePictureImageView = (ImageView) findViewById(R.id.no_profile_picture);
        firstNameEditText = (TextView) findViewById(R.id.firstNameEditText);
        lastNameEditText = (TextView) findViewById(R.id.lastNameEditText);
        heightEditText = (TextView) findViewById(R.id.heightEditText);
        weightEditText = (TextView) findViewById(R.id.weightEditText);
        goalEditText = (TextView) findViewById(R.id.goalEditText);
        mostFrequentTextView = (TextView) findViewById(R.id.mostFrequentTextView);
    }

    class UserInfoFromServer extends AsyncTask<Void,Void,Void> {
        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null, ps1=null, ps2=null;
            ResultSet rs, rs1,mostFrequentWorkout;
            Connection conn = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("SELECT firstName, lastName, goal, height, weight " +
                        "FROM LoggedUser where username =?");
                ps.setString(1,username);
                rs = ps.executeQuery();
                while(rs.next()){
                    firstName = rs.getString("firstName");
                    lastName = rs.getString("lastName");
                    goal = rs.getString("goal");
                    height = rs.getInt("height");
                    weight = rs.getInt("weight");
                }
                rs.close();

                ps1 = conn.prepareStatement("SELECT image FROM LoggedUserImage where " +
                        "username =?");
                ps1.setString(1,username);
                rs1 = ps1.executeQuery();
                while(rs1.next()){
                    Blob blob = rs1.getBlob("image");
                    int blobLength = (int) blob.length();
                    profilePictureAsBytes = blob.getBytes(1,blobLength);
                    profilePictureAsBitmap = BitmapFactory.decodeByteArray(profilePictureAsBytes, 0, profilePictureAsBytes.length);
                }

                ps2 = conn.prepareStatement("SELECT workoutName, workoutFrequency FROM" +
                        " Workout, User_Workout WHERE Workout.workoutID = User_Workout.workoutID AND username = ? ORDER BY workoutFrequency" +
                        " DESC LIMIT 1");
                ps2.setString(1,username);
                mostFrequentWorkout = ps2.executeQuery();
                if(mostFrequentWorkout.next()){
                    Log.d("Most Freq",mostFrequentWorkout.getString("workoutName"));
                    mostFrequentWorkoutname = mostFrequentWorkout.getString("workoutName");
                }

            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }

        protected void onPostExecute(Void result){
            if(profilePictureAsBytes!=null) {
                Bitmap bmp = BitmapFactory.decodeByteArray(profilePictureAsBytes, 0, profilePictureAsBytes.length);
                profilePictureImageView.setImageBitmap(bmp);
            }
            firstNameEditText.setText(firstName);
            lastNameEditText.setText(lastName);
            heightEditText.setText(String.valueOf(height));
            weightEditText.setText(String.valueOf(weight));
            goalEditText.setText(goal);
            mostFrequentTextView.setText(mostFrequentWorkoutname);
            setTitle(firstName + " " + lastName + "'s Profile");
        }
    }

}
