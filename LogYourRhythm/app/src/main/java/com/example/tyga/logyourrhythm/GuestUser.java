package com.example.tyga.logyourrhythm;

import java.io.Serializable;
import java.sql.Time;
import java.util.*;


public class GuestUser extends User implements Serializable{
    public static final long serialVersionUID = 1;
    protected WorkoutManager workoutManager;
    protected activityManager activityManager;
    protected String username;

    public GuestUser () {
        //hard coded here.
        this.username = "Guest";
        workoutManager = new WorkoutManager();
        activityManager = null;
        userType = false;
    }

    public WorkoutManager getWorkoutManager() {return workoutManager;}

    void startWorkout(Workout workoutToStart) {
        workoutManager.startWorkout(workoutToStart);
    }

    public String getUsername(){
        return username;
    }
    //not sure if I should open full access of guest user's workoutmanager.. there is potential risk that a guest user gain full access of workout.
}