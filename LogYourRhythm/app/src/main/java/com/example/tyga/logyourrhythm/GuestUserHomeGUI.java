package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.concurrent.ExecutionException;

public class GuestUserHomeGUI extends AppCompatActivity {
    private GuestUser guestUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_user_home_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupListView();

        Bundle extras = getIntent().getExtras();
        guestUser = (GuestUser) extras.getSerializable("GuestUser");

        try{
            (new UserActivitiesWorkoutsFromServer(guestUser, "")).execute().get();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }catch(ExecutionException ee){
            ee.printStackTrace();
        }
    }

    private void setupListView(){
        ListView guestOptions = (ListView) findViewById(R.id.guestOptionsList);
        String [] options = new String[] {"My Workouts"};
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,options);
        guestOptions.setAdapter(adapter);
        guestOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View viewClicked, int pos, long id) {
                //wasn't too sure about where to take the guest user
                //MyWorkoutGUI would need to know the user is a guest and not allow
                //the guest to add any workouts or activities
                Intent intent = new Intent(GuestUserHomeGUI.this, MyWorkoutsGUI.class);
                intent.putExtra("UserSpec", "guestUser");
                intent.putExtra("GuestUser", guestUser);
                startActivity(intent);
            }
        });
    }

    public void goToRegister(View v){
        Intent intent = new Intent(GuestUserHomeGUI.this,RegisterGUI.class);
        intent.putExtra("GuestUser", guestUser);
        intent.putExtra("UserSpec","guestUser");
        startActivity(intent);
        finish();
    }
}
