package com.example.tyga.logyourrhythm;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tyga on 4/25/2016.
 */
public class LoggedHomeAdapter extends ArrayAdapter<String> {

    private String[] userMenus;

    public LoggedHomeAdapter(Context context, int textViewResourceId, String[] menus) {
        super(context, textViewResourceId, menus);
        userMenus = menus;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.custom_list_view, parent, false);
        }
        String userChoice = userMenus[position];
        if (userChoice != null) {
            TextView activityName = (TextView) v.findViewById(R.id.activityname);
            TextView activityInfo = (TextView) v.findViewById(R.id.activityinfo);
            ImageView activityIcon = (ImageView) v.findViewById(R.id.activityicon);
            if (userChoice != null) {
                activityName.setText(userChoice);
            }
            //Setting icon for each activity
            Drawable drawable = null;
            if (userChoice.equals("My Profile")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.profile);
            } else if (userChoice.equals("My Workouts")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.workouts);
            } else if (userChoice.equals("My Progress")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.progress);
            } else if (userChoice.equals("My Friends")) {
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.friends);
            }else drawable = ContextCompat.getDrawable(getContext(), R.drawable.logout);
            if (drawable != null) activityIcon.setImageDrawable( drawable );
        }
        return v;
    }
}
