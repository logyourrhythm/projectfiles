package com.example.tyga.logyourrhythm;

import android.app.ActivityManager;

import java.io.Serializable;
import java.sql.Time;
import java.util.*;
import java.lang.Integer;

public class LoggedUser extends User implements Serializable{

    public static final long serialVersionUID = 12;
    //ProgressManager progressManager;
    private String firstName, lastName, username;

    private int height;
    private int weight;
    private int age;
    private String goal;

    //Server server;
    private Vector<LoggedUser> friends;

    private WorkoutManager wManager;
    private activityManager aManager;
    private ProgressManager pManager;

    public LoggedUser (String username){
        this.username=username;
//        workoutManager = new WorkoutManager();
//        activityManager = new activityManager(this);
        //Here we may need a constructor for activityManager without parameter passed in?
        friends = new Vector<LoggedUser>();
        pManager = new ProgressManager();
        userType = true;
    }
    /*
    public void setServer(Server server) {
        this.server = server;
    }
    */

    public String getFirstName (){return firstName;}

    public void setFirstName(String fn){firstName = fn;}

    public String getLastName() {return lastName;}

    public void setLastName(String ln){lastName = ln;}

    public int getHeight () {return height;}

    public void setHeight(String h) {
        try {
            height = Integer.parseInt(h);
        }
        catch(NumberFormatException e) {
            //not a double change nothing
        }
    }

    public int getWeight() {return weight;}

    public void setWeight(String w) {
        try {
            weight = Integer.parseInt(w);
        }
        catch(NumberFormatException e) {
            //not a double change nothing
        }
    }

    public int getAge () {return age;}

    public void setAge(String a) {
        if(isInteger(a)){
            age = Integer.parseInt(a);
        }
    }

    public String getGoal () {return goal;}

    public void setGoal(String g){goal = g;}

    public Vector<LoggedUser> getFriends(){
        return friends;
    }

    public void setWorkoutManager(WorkoutManager wm) {wManager = wm;}

    public activityManager getActivityManager() {return aManager;}

    public void setActivityManager(activityManager am){aManager = am;}

    public void addFriend(LoggedUser userToAdd){
        friends.add(userToAdd);
    }

    public WorkoutManager getWorkoutManager() {return wManager;}

    public ProgressManager getProgressManager() {return pManager; }

    public void setProgressManager( ProgressManager pm) {pManager = pm;}

    void startWorkout(Workout workoutToStart) {
        wManager.startWorkout(workoutToStart);
    }

    //check if string is a number for input
    private static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    private static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

    public String getUsername(){
        return username;
    }
}