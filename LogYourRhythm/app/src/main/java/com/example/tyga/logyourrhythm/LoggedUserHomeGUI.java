package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

public class LoggedUserHomeGUI extends AppCompatActivity {
    private LoggedUser user;
    private ListView userOptions;
    private String [] options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loggeduserhomegui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //get user from intent
        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser)bundle.getSerializable("LoggedUser");
        WorkoutManager wm;
        if(user.getWorkoutManager()==null){
            Log.d("WM NULL", "WM NULL");
        }
        wm = user.getWorkoutManager();
        Vector<Workout> workouts = wm.getUserWorkouts();
        Vector<Activity> activities;
        for(Workout w: workouts){
            activities = w.getActivitiesList();
            Log.d("ACTIVTY LIST SIZE", String.valueOf(w.getActivitiesList().size()));
            for(Activity a: activities){
                Log.d("ACTIVITY LIST", a.getName());
            }
        }
        setupListView();
        createListViewActions();
        try{
            getUserFriendsFromServer();
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }catch(ExecutionException ee){
            ee.printStackTrace();
        }
    }

    private void getUserFriendsFromServer() throws InterruptedException, ExecutionException{
        (new UserFriendsFromServer(user.getUsername())).execute().get();
    }

    //sets up the list of options for users to choose
    private void setupListView(){
        userOptions = (ListView) findViewById(R.id.userOptionsList);
        options = new String[] {"My Profile","My Workouts","My Progress","My Friends","Logout"};
        LoggedHomeAdapter adapter = new LoggedHomeAdapter(this, android.R.layout.simple_list_item_1,options);
        userOptions.setAdapter(adapter);
    }

    private void createListViewActions(){
        userOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long id) {

                String choice =options[position];
                if(choice.equals("My Profile")){
                    //go to profile page
                    Intent intent = new Intent(LoggedUserHomeGUI.this, UserProfileGUI.class);
                    intent.putExtra("LoggedUser", user);
                    startActivity(intent);
                }else if(choice.equals("My Workouts")){
                    //go to workouts page
                    Log.d("In My Workouts", "Go to My Workouts");
                    Intent intent = new Intent(LoggedUserHomeGUI.this,MyWorkoutsGUI.class);
                    intent.putExtra("UserSpec","loggedUser");
                    intent.putExtra("LoggedUser",user);
                    startActivityForResult(intent,RequestCodes.MyWorkoutsGUI);
                }else if(choice.equals("My Progress")){
                    // go to user's progress page
                    Log.d("In My Activity","Go to My Progress");
                    Intent intent = new Intent(LoggedUserHomeGUI.this,ProgressMenuGUI.class);
                    intent.putExtra("LoggedUser",user);
                    startActivityForResult(intent, RequestCodes.MyProgressGUI);
                }else if(choice.equals("My Friends")){
                    Log.d("In My Friends", "Go to My Friends");
                    Intent intent = new Intent(LoggedUserHomeGUI.this, UserFriendListGUI.class);
                    intent.putExtra("LoggedUser", user);
                    startActivityForResult(intent,RequestCodes.viewMyFriends);
                }else{
                    //go back to login page
                    finish();
                }

            }
        });
    }

//    private void createListViewActions(){
//        ListView list = (ListView) findViewById(R.id.userOptionsList);
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
//            @Override
//            public void onItemClick(AdapterView<?> parent, View viewClicked, int pos, long id){
//                TextView textClicked = (TextView) viewClicked;
//                if(pos==0){
//                    //go to profile page
//                    Intent intent = new Intent(LoggedUserHomeGUI.this, UserProfileGUI.class);
//                    intent.putExtra("LoggedUser", user);
//                    startActivity(intent);
//                }else if(pos==1){
//                    //go to workouts page
//                    Log.d("In My Workouts", "Go to My Workouts");
//                    Intent intent = new Intent(LoggedUserHomeGUI.this,MyWorkoutsGUI.class);
//                    intent.putExtra("UserSpec","loggedUser");
//                    intent.putExtra("LoggedUser",user);
//                    startActivityForResult(intent,RequestCodes.MyWorkoutsGUI);
//                }else if(pos==2){
//                    // go to user's progress page
//                    Log.d("In My Activity","Go to My Progress");
//                    Intent intent = new Intent(LoggedUserHomeGUI.this,ProgressMenuGUI.class);
//                    intent.putExtra("LoggedUser",user);
//                    startActivityForResult(intent, RequestCodes.MyProgressGUI);
//                }else if(pos==3){
//                    Log.d("In My Friends", "Go to My Friends");
//                    Intent intent = new Intent(LoggedUserHomeGUI.this, UserFriendListGUI.class);
//                    intent.putExtra("LoggedUser", user);
//                    startActivityForResult(intent,RequestCodes.viewMyFriends);
//                }else{
//                    //go back to login page
//                    finish();
//                }
//            }
//        });
//    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == RequestCodes.MyWorkoutsGUI || requestCode == RequestCodes.MyProgressGUI
                || requestCode == RequestCodes.viewMyFriends)
                && resultCode == RESULT_OK && data != null) {
            user = (LoggedUser) data.getSerializableExtra("LoggedUser");
        }
    }

    class UserFriendsFromServer extends AsyncTask<Void,Void,Void>{
        private String username;
        public UserFriendsFromServer(String username){
            this.username = username;
        }

        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null;
            ResultSet rs;
            Connection conn = null;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("SELECT username2 FROM UserFriends where username1 = ?");
                ps.setString(1,username);
                rs = ps.executeQuery();
                while(rs.next()){
                    user.addFriend(new LoggedUser(rs.getString("username2")));
                }
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }
    }
}
