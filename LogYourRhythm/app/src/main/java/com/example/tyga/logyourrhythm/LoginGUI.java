package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class LoginGUI extends AppCompatActivity{

    private EditText login_username_text,login_password_text;
    private Button login_button, register_button, guest_button;
    private TextView register_link;
    private boolean usernameExists,goodPassword;
	private String username, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logingui);
        login_username_text = (EditText) findViewById(R.id.login_username_text); //gets text field for username from GUI
        login_password_text = (EditText) findViewById(R.id.login_password_text); //gets text field for password from GUI

        login_button = (Button) findViewById(R.id.login_button); //gets login button from GUI
        register_button = (Button) findViewById(R.id.register_button); //gets register button from GUI
        guest_button = (Button) findViewById(R.id.guest_button); //gets continue as guest button from gUI
    }

    public void checkValidCredentials(View v){ //an onClick spec from content_loginguigui.xml calls this to check username
        //passes in username and password entered to the asynctask that will check mysql
        (new ValidCredentialsCheck(login_username_text.getText().toString(),login_password_text.getText().toString(),this)).execute();
    }

    public void registerNewUser(View v){ //an onClick spec from content_loginguigui.xml calls this to take user to RegisterGUI GUI
        Intent intent = new Intent(LoginGUI.this,RegisterGUI.class);
        startActivity(intent);
    }

    public void goToHomePage(){ //goes to home page for LoggedUser after validating credentials
        LoggedUser user = new LoggedUser(login_username_text.getText().toString());
        try {
            (new UserActivitiesWorkoutsFromServer(user,"")).execute().get();
            Intent intent = new Intent(LoginGUI.this, LoggedUserHomeGUI.class);
            intent.putExtra("LoggedUser", user);
            startActivity(intent);
            finish();
        }catch(InterruptedException ie){
            Log.d("IE", ie.getMessage());
        }catch (ExecutionException ee){
            Log.d("EE", ee.getMessage());
        }
    }

    public void goToGuestHomePage(View v){
        GuestUser user = new GuestUser();
        Intent intent = new Intent(LoginGUI.this,GuestUserHomeGUI.class);
        intent.putExtra("GuestUser", user);
        startActivity(intent);
        finish();
    }
}
