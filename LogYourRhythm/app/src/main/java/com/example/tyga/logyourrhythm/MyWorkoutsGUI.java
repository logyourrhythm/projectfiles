package com.example.tyga.logyourrhythm;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class MyWorkoutsGUI extends Activity implements AdapterView.OnItemClickListener{
    //DATA MEMBERS
    ListView l;
    Vector<Workout> workouts;
    Vector<String> workoutNames;
    private GuestUser guestUser;
    private LoggedUser loggedUser;
    private String userType;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> array;

    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_workouts);

        //get user information passed into
        Bundle bundle = getIntent().getExtras();
        userType = (String) bundle.getString("UserSpec");
        if (userType.equals("guestUser")) {
            guestUser = (GuestUser) bundle.getSerializable("GuestUser");
            loggedUser = null;
            workouts = guestUser.getWorkoutManager().getUserWorkouts();
        } else if (userType.equals("loggedUser")) {
            loggedUser = (LoggedUser) bundle.getSerializable("LoggedUser");
            guestUser = null;
            workouts = loggedUser.getWorkoutManager().getUserWorkouts();
        }
        workoutNames = new Vector<String>();
        for(Workout w: workouts){
            workoutNames.add(w.getWorkoutName());
        }
        if (workoutNames.size() == 0) {
            Toast.makeText(this, "No workouts to view!", Toast.LENGTH_SHORT).show();
        }
        l = (ListView) findViewById(R.id.listView); //cast to listview object
        //convert our data using an array adapter
        //parameters are this, how to convert the list view
        array = new ArrayList<String>(workoutNames);
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        //set the listview adapter
        l.setAdapter(arrayAdapter);
        l.setOnItemClickListener(this);

        //Floating action button: this will take the user to create a new workout
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userType.equals("guestUser")) {
                    //if user is a guest, they cannot add a new workout
                    AlertDialog.Builder builder = new AlertDialog.Builder(MyWorkoutsGUI.this);
                    builder.setMessage("Create an account with Log Your Rhythm to start adding custom workouts!")
                            .setTitle("Action Not Available!");
                    builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (userType.equals("loggedUser")){
                    Intent intent = new Intent(MyWorkoutsGUI.this,CreateNewWorkoutGUI.class);
                    intent.putExtra("LoggedUser", loggedUser);
                    startActivityForResult(intent, RequestCodes.newWorkout);
                }
            }
        });
    }

    //METHODS
    @Override
    //1 = parent, 2 = listview the user selected, 3 = index position of textview
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        final int pos = i;
        final TextView temp = (TextView)view;
        AlertDialog.Builder builder = new AlertDialog.Builder(MyWorkoutsGUI.this);
        builder.setMessage("What would you like to do?")
                .setTitle(temp.getText());
        // Add the buttons
        builder.setPositiveButton("Start", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(MyWorkoutsGUI.this, CurrentWorkoutGUI.class);
                Workout w = null;
                if (userType.equals("loggedUser")) {
                    intent.putExtra("LoggedUser", loggedUser);
                    intent.putExtra("UserType", "loggedUser");
                    w = loggedUser.getWorkoutManager().getWorkoutByName(temp.getText().toString());
                    intent.putExtra("Workout", w);
                    startActivityForResult(intent, RequestCodes.finishedWorkout);
                } else {
                    intent.putExtra("GuestUser", guestUser);
                    intent.putExtra("UserType", "guestUser");
                    w = guestUser.getWorkoutManager().getWorkoutByName(temp.getText().toString());
                    intent.putExtra("Workout", w);
                    startActivity(intent);
                }
            }
        });
        if (userType.equals("loggedUser")) {
            builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Toast.makeText(MyWorkoutsGUI.this, temp.getText() + " was deleted", Toast.LENGTH_SHORT).show();
                    array.remove(pos);
                    arrayAdapter.notifyDataSetChanged();
                    arrayAdapter.notifyDataSetInvalidated();
                    //remove workout from user
                    loggedUser.getWorkoutManager().removeWorkout(temp.getText().toString());
                    //Check if this is in the progress manager and if so, remove it there as well
                    Workout w = loggedUser.getWorkoutManager().getWorkoutByName(temp.getText().toString());
                    if (loggedUser.getProgressManager().getWorkoutToActivities().containsKey(w)) {
                        loggedUser.getProgressManager().removeProgress(w);
                    }
                    //Also remove this workout from the data base
                    (new RemoveWorkoutFromServer(temp.getText().toString(),loggedUser)).execute();
                }
            });
        } else { //guest users may not delete workouts
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        //Allow user to simply view the activities contained in a workout
        builder.setNeutralButton("View", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(MyWorkoutsGUI.this, ViewWorkoutGUI.class);
                if (userType.equals("loggedUser")) {
                    Workout w = loggedUser.getWorkoutManager().getWorkoutByName(temp.getText().toString());
                    intent.putExtra("Workout", w);
                } else {
                    Workout w = guestUser.getWorkoutManager().getWorkoutByName(temp.getText().toString());
                    intent.putExtra("Workout", w);
                }

                startActivity(intent);
            }
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    //Handling on return actions
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //action 1: returning from creating a new workout
        if (requestCode == RequestCodes.newWorkout && resultCode == RESULT_OK && data != null) {
            loggedUser = (LoggedUser) data.getSerializableExtra("LoggedUser");
            String newWorkoutName = (String) data.getStringExtra("NewWorkoutName");
            Toast.makeText(MyWorkoutsGUI.this, newWorkoutName + " was created", Toast.LENGTH_LONG).show();
            array.add(newWorkoutName);
            arrayAdapter.notifyDataSetChanged();
        } else if (requestCode == RequestCodes.finishedWorkout && resultCode == RESULT_OK) {
            loggedUser = (LoggedUser) data.getSerializableExtra("LoggedUser");
            Toast.makeText(MyWorkoutsGUI.this, "Workout completed!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        //if the current user is a logged user, pass the user back to the main menu
        if (loggedUser != null) {
            Intent i = new Intent(MyWorkoutsGUI.this, LoggedUserHomeGUI.class);
            i.putExtra("LoggedUser", loggedUser);
            i.setFlags(i.FLAG_ACTIVITY_SINGLE_TOP);
            i.setFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
            setResult(RESULT_OK, i);
        }
        super.onBackPressed();
    }

}
