package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/9/2016.
 */
public class NewUserToServer extends AsyncTask<Void,Void,Void> {
    private String passwordToHash,username;
    private RegisterGUI registerGUI;
    public NewUserToServer(String username, String passwordToHash, RegisterGUI registerGUI){
        this.username = username;
        this.passwordToHash = passwordToHash;
        this.registerGUI = registerGUI;
    }
    @Override
    protected Void doInBackground(Void ... params){
        PreparedStatement ps = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("INSERT INTO LoggedUser(username,password) VALUES(?,?)");
            ps.setString(1, username);
            ps.setString(2,BCrypt.hashpw(passwordToHash,BCrypt.gensalt(12)));
            ps.execute();
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
       registerGUI.goToUserInfoPage();
    }
}
