package com.example.tyga.logyourrhythm;

import android.os.AsyncTask;
import android.util.Log;

import java.security.Timestamp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.xml.transform.Result;

/**
 * Created by Tyga on 4/10/2016.
 */
public class NewWorkoutToServer extends AsyncTask<Void,Void,Void>{
    private Vector<Activity> activities; //need to store all activities in a workout
    private Workout workout; //need to get workout to store workout
    private LoggedUser user; //need to get user so we have their username
    public NewWorkoutToServer(Workout workout, Vector<Activity> activities, LoggedUser user){
        this.workout = workout;
        this.activities = activities;
        this.user = user;
    }

    @Override
    protected Void doInBackground(Void ... params){
        PreparedStatement ps = null, ps1 = null, ps2 = null, ps3 = null;
        Connection conn = null;
        ResultSet mostRecentActivities = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("INSERT INTO Workout(whenCreated,workoutName) VALUES(?,?)");
            ps.setTimestamp(1,new java.sql.Timestamp(new java.util.Date().getTime()));
            ps.setString(2, workout.getWorkoutName());
            ps.execute();

            ps1 = conn.prepareStatement("INSERT INTO User_Workout(username, workoutID) VALUES(?," +
                    "(SELECT workoutID FROM Workout ORDER BY workoutID DESC LIMIT 1)");
            ps1.setString(1,user.getUsername());
            ps1.execute();

            ps2 = conn.prepareStatement("SELECT activityID FROM User_Activity WHERE username=? ORDER BY activityID DESC LIMIT ?");
            ps2.setString(1,user.getUsername());
            ps2.setInt(2, activities.size());
            mostRecentActivities = ps2.executeQuery();

            ps3 = conn.prepareStatement("INSERT INTO Workout_Activity(workoutID,activityID) VALUES " +
                    "((SELECT workoutID FROM User_Workout where username = ? ORDER BY workoutID DESC LIMIT 1),?)");
            while(mostRecentActivities.next()){
                ps3.setString(1,user.getUsername());
                ps3.setInt(2, mostRecentActivities.getInt("activityID"));
                ps3.execute();
            }
            mostRecentActivities.close();
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(ps1!=null) ps1.close();
                if(ps2!=null) ps2.close();
                if(ps3!=null) ps3.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        //take you back to MyWorkoutsGUI probably.
        // This is after you add the workout to the server, so I think that would make sense
    }
}
