package com.example.tyga.logyourrhythm;


import java.sql.Time;
import java.util.HashMap;

public class Progress {
    //DATA MEMBERS
    private Activity activity;
    private Integer goal;
    private String name;
    private HashMap<Long, Integer> progressData;

    //CONSTRUCTOR
    public Progress(Activity activity) {
        this.activity = activity;
        name = activity.getName();
        progressData = activity.getData();
        goal = null;
    }

    //METHODS
    public HashMap<Long, Integer> getProgressData() {
        return progressData;
    }

    public Integer getGoal() {
        return goal;
    }

    public void setGoal(Integer goal) {
        this.goal = goal;
    }

    public String getName() {
        return name;
    }
}
