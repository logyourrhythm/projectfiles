package com.example.tyga.logyourrhythm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class ProgressItemGUI extends AppCompatActivity {

    private GraphView graph;
    private Spinner timeSpinner;
    private TextView progressItemName, goalUnits;
    private EditText goalName;
    private Button editGoalButton;
    private HashMap<Long, Integer> data;
    private DataPoint [] dataPoints;
    ArrayAdapter<CharSequence> timeAdapter;
    private LineGraphSeries<DataPoint> series;
    private Context context;
    private Activity activity;
    private String boundaries = "1 month"; //0 is 1 month, 1 is 3 months, 2 is 6 months, 3 is 1 year
    private int offset = 2619227; //1 month offset initially

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_item_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;

        Bundle bundle = getIntent().getExtras();
        activity = (Activity)bundle.getSerializable("ProgressActivity");
        //activity = new Activity("Swerve", "Miles", "Distance");
        /*activity.addData((System.currentTimeMillis()/1000)-2972227, 10);
        activity.addData((System.currentTimeMillis()/1000)-2132627, 20);
        activity.addData((System.currentTimeMillis()/1000)-1882617, 30);
        activity.addData((System.currentTimeMillis()/1000)-1412627, 40);
        activity.addData((System.currentTimeMillis()/1000)-12000, 50);*/

        graph = (GraphView)findViewById(R.id.graph);
        progressItemName = (TextView)findViewById(R.id.progressItemName);
        goalUnits = (TextView)findViewById(R.id.goalUnits);
        goalName = (EditText)findViewById(R.id.goalName);
        editGoalButton = (Button)findViewById(R.id.button);
        timeSpinner = (Spinner)findViewById(R.id.spinner);
        timeAdapter = ArrayAdapter.createFromResource(this, R.array.timeArray, android.R.layout.simple_spinner_item);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(timeAdapter);

        //setting the activity name and goal name depending on which activity is passed through
        progressItemName.setText(activity.getName());
        if(activity.getGoal() != null) {
            System.out.println("Activity goal: "+ activity.getGoal());
            goalName.setText(activity.getGoal().toString());
        }
        goalUnits.setText(activity.getUnits());

        editGoalButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goalName.setFocusableInTouchMode(true);
            }
        });

        goalName.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    activity.setGoal(Integer.parseInt(goalName.getText().toString()));
                    graph.removeAllSeries();
                    createGraph();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    return true;
                }
                return false;
            }
        });

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boundaries = parent.getSelectedItem().toString();
                if (boundaries.equals("1 week")) {
                    offset = 604800;//86400;//2619227;
                } else if (boundaries.equals("1 month")) {
                    offset = 2619227;
                } else if (boundaries.equals("6 months")) {
                    offset = 15778476;
                } else if (boundaries.equals("1 year")) {
                    offset = 31556952;
                }
                createGraph();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void createGraph() {
        data = activity.getData();
        Vector<DataPoint> dataVector = new Vector<DataPoint>();
        if(data.size() == 0) {
            return;
        }

        Iterator it = data.entrySet().iterator();
        Integer i = 0;
        while(it.hasNext()) { //iterate through data and add each pair to a DataPoint[]
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + "  " + pair.getValue());
            Date date = new Date((Long)pair.getKey()); //converting seconds to milliseconds
            if((long)pair.getKey() * 1000 >= (((System.currentTimeMillis()/1000)-offset)*1000) ) { //if the value is between current time and the boundaries
                //check the math on this ^
                System.out.println("Well, at least I found some valid data :)");
                //dataPoints[i] = new DataPoint(date,(int)pair.getValue());
                dataVector.add(new DataPoint(date, (int)pair.getValue()));
            }
            i++;
        }
        dataPoints = new DataPoint[dataVector.size()];
        for(int j = 0; j < dataVector.size(); j++) {
            Double y = dataVector.elementAt(j).getY();
            int y1 = y.intValue();
            DataPoint newData = new DataPoint(dataVector.elementAt(j).getX(), y1);
            //dataPoints[j] = dataVector.elementAt(j);
            dataPoints[j] = newData;
        }
        Arrays.sort(dataPoints, new Comparator<DataPoint>() {
            public int compare(DataPoint a, DataPoint b) {
                int xComp = Double.compare(a.getX(), b.getX());
                return xComp;
            }
        });
        for(int j = 0; j < dataVector.size(); j++) {
            System.out.println("index: " + j + "... "+dataPoints[j].getX()+"   "+dataPoints[j].getY());
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(dataPoints); //what should be used. above is hardcoded debugging version
        System.out.println("Series: " + series);
        for(int j = 0; j < dataVector.size(); j++) {
            System.out.println("index: " + j + "... "+dataPoints[j].getX()+"   "+dataPoints[j].getY());
        }
        if(activity.getGoal() != null) {
            LineGraphSeries<DataPoint> series1 = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(System.currentTimeMillis(), activity.getGoal()),
                    new DataPoint(((System.currentTimeMillis() / 1000) - offset) * 1000, activity.getGoal())
            });
            //making a red dotted line for the goal line
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(10);
            paint.setPathEffect(new DashPathEffect(new float[]{20, 10}, 0));
            paint.setColor(Color.RED);
            series1.setCustomPaint(paint);
            graph.addSeries(series1);
        }

        graph.addSeries(series);
        //http://www.android-graphview.org/documentation/label-formatter
        // set date label formatter
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this));
        graph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 4 because of the space... still have to add month, 3 months...
        // set manual x bounds to have nice steps
        //graph.getViewport().setMinX(date.getTime());
        //graph.getViewport().setMaxX(date4.getTime());
        graph.getViewport().setMinX(((System.currentTimeMillis() / 1000) - offset) * 1000); //current time minus 1 month/3months
        graph.getViewport().setMaxX(System.currentTimeMillis());
        graph.getViewport().setXAxisBoundsManual(true);
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("ProgressActivity", activity);
        setResult(RESULT_OK, resultIntent);
        finish();
        super.onBackPressed();
    }
}
