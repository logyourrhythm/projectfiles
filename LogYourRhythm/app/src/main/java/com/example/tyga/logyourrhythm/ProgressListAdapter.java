package com.example.tyga.logyourrhythm;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tiffany on 4/17/2016.
 */
public class ProgressListAdapter extends ArrayAdapter<Activity> {
    private ArrayList<Activity> progressActivites;

    public ProgressListAdapter(Context context, int textViewResourceId, ArrayList<Activity> progressActivites) {
        super(context, textViewResourceId, progressActivites);
        this.progressActivites = progressActivites;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.custom_progress_list_view, parent, false);
        }
        Activity activity = progressActivites.get(position);
        if (activity != null) {
            TextView progressName = (TextView) v.findViewById(R.id.progressname);
            TextView progressWorkoutName = (TextView) v.findViewById(R.id.progressworkoutname);
            if (progressName != null) {
                progressName.setText(activity.getName());
            }
            if(progressWorkoutName != null) {
                progressWorkoutName.setText("From workout: " + activity.getWorkoutName());
            }
        }
        return v;
    }


}
