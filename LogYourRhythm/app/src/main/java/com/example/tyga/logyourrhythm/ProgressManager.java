package com.example.tyga.logyourrhythm;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

public class ProgressManager implements Serializable {
    //DATA MEMBERS
    public static final long serialVersionUID=1;

    // all activities being tracked
    private Vector<Activity> progressItems;
    //maps workout to activities in the workout which are tracked
    private HashMap<Workout, Vector<Activity>> workoutToActivities;


    //CONSTRUCTOR
    public ProgressManager() {
        workoutToActivities = new HashMap<Workout, Vector<Activity>>();
        progressItems = new Vector<Activity>();
    }


    //METHODS
    //add progress item
    public void addProgressItem(Activity progressItem, Workout workout) {
        progressItems.add(progressItem);
        if (workoutToActivities.containsKey(workout)) {
            Vector<Activity> progressActivitiesList = workoutToActivities.get(workout);
            progressActivitiesList.add(progressItem);
            workoutToActivities.put(workout, progressActivitiesList);
        } else {
            Vector<Activity> progressActivitiesList = new Vector<Activity>();
            progressActivitiesList.add(progressItem);
            workoutToActivities.put(workout, progressActivitiesList);
        }
    }

    //update an activity related to a workout
    public void updateProgressItem(Activity activity, String workoutName) {
        for (Workout w : workoutToActivities.keySet()) {
            if (w.getWorkoutName().equals(workoutName)) {
                Vector<Activity> workoutActivities =  workoutToActivities.get(w);
                for (Activity a : workoutActivities) {
                    if (a.getName().equals(activity.getName())) {
                        workoutActivities.remove(a);
                        workoutActivities.add(activity);
                        break;
                    }
                }
                break;
            }
        }
        int i = 0;
        for (Activity a : progressItems) {
            if (a.getName().equals(activity.getName()) && a.getWorkoutName().equals(activity.getWorkoutName())) {
                progressItems.set(i, activity);
            }
            i +=1;
        }
    }

    //set progress list
    public void setProgresses(Vector<Activity> progressItems) {
        this.progressItems = progressItems;
    }
    //get progress list
    public Vector<Activity> getProgresses() {
        return progressItems;
    }

    //set workout to progress/activity hashmap
    public void setWorkoutToActivities(HashMap<Workout, Vector<Activity>> workoutToActivities) {
        this.workoutToActivities = workoutToActivities;
    }
    //get workout to progress/activity hashmap
    public HashMap<Workout, Vector<Activity>> getWorkoutToActivities() {
        return workoutToActivities;
    }

    //remove progress items that are associated with a workout
    public void removeProgress(Workout w) {
        workoutToActivities.remove(w);
        for (Activity a : progressItems) {
            if (a.getWorkoutName().equals(w.getWorkoutName())) {
                progressItems.remove(a);
                return;
            }
        }
    }

}
