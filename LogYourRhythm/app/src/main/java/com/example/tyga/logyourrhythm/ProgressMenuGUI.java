package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class ProgressMenuGUI extends AppCompatActivity implements AdapterView.OnItemClickListener {
    //DATA MEMBERs
    private LoggedUser user; //only logged users can see the progress menu
    private ListView l;
    private Vector<String> progressItemNames; //this
    private Vector<Activity> progressItems; //      and this are parallel vectors-names and items should directly correspond
    private ProgressListAdapter arrayAdapter;
    private ArrayList<Activity> array;


    //CONSTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_menu_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser) bundle.getSerializable("LoggedUser");

        //FLOATING ACTION BUTTON-use this to add a new progress item
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.getWorkoutManager().getUserWorkouts().size() == 0) {
                    //if the user doesn't have any progress items to track, don't advance to next screen
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProgressMenuGUI.this);
                    builder.setMessage("No available activities to track! " +
                            "Start creating new workouts to begin tracking progress.");
                    builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Intent intent = new Intent(ProgressMenuGUI.this, AddProgressGUI.class);
                    intent.putExtra("LoggedUser", user);
                    startActivityForResult(intent, RequestCodes.trackingNewProgress);
                }
            }
        });

        //LIST VIEW-lists all activities with progress currently being tracked
        progressItems = user.getProgressManager().getProgresses();
        if (progressItems.size() == 0) {
            Toast.makeText(this, "No tracked progress activities to view!", Toast.LENGTH_SHORT).show();
            array = new ArrayList<Activity>();
        } else array = new ArrayList<>(progressItems);
        l = (ListView) findViewById(R.id.progressListView);
        arrayAdapter = new ProgressListAdapter(this, R.layout.custom_progress_list_view, array);
        //set the listview adapter
        l.setAdapter(arrayAdapter);
        l.setOnItemClickListener(this);
    }

    //METHODS
    //This method connects the progress items listed on the screen to the indiviual screens for each progress item
    @Override
    //1 = parent, 2 = listview the user selected, 3 = index position of textview
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        //start the progress item gui
        Intent intent = new Intent(ProgressMenuGUI.this, ProgressItemGUI.class);
        intent.putExtra("ProgressActivity", progressItems.elementAt(i));
        startActivityForResult(intent, RequestCodes.viewProgressItem);
    }


    //Method for handling actions upon returing to this screen
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //action 1: returning from creating a new workout
        if (requestCode == RequestCodes.trackingNewProgress && resultCode == RESULT_OK && data != null) {
            user = (LoggedUser) data.getSerializableExtra("LoggedUser");
            Activity activity = (Activity) data.getSerializableExtra("ProgressActivity");
            progressItems.add(activity);
            array.add(activity);
            arrayAdapter.notifyDataSetChanged();
            //now take the user to see their progress item
//            Toast.makeText(ProgressMenuGUI.this, activity.getName() + " is now being tracked.", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(ProgressMenuGUI.this, ProgressItemGUI.class);
            intent.putExtra("ProgressActivity", activity);
            startActivityForResult(intent, RequestCodes.viewProgressItem);
        } else if (requestCode == RequestCodes.viewProgressItem && resultCode == RESULT_OK && data != null) {
            //add progreess activity back to user, to account for change if the user decided to add/edit a goal
            Activity activity = (Activity) data.getSerializableExtra("ProgressActivity");
            (new UpdateUserGoal(user.getUsername(),activity)).execute();
            ProgressManager pm = user.getProgressManager();
            pm.updateProgressItem(activity, activity.getWorkoutName());
            user.setProgressManager(pm);
            user.getActivityManager().updateActivity(activity);
            user.getWorkoutManager().updateWorkoutActivity(activity);
        }
    }

    @Override
    public void onBackPressed() {
        //if the current user is a logged user, pass the user back to the main menu
        if (user != null) {
            Intent i = new Intent(ProgressMenuGUI.this, LoggedUserHomeGUI.class);
            i.putExtra("LoggedUser", user);
            i.setFlags(i.FLAG_ACTIVITY_SINGLE_TOP);
            i.setFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
            setResult(RESULT_OK, i);
        }
        super.onBackPressed();
    }

    class UpdateUserGoal extends AsyncTask<Void,Void,Void> {
        private String username;
        private Activity curr_activity;

        public UpdateUserGoal(String username, Activity curr_activity) {
            this.username = username;
            this.curr_activity = curr_activity;
        }

        private int getActivityID() {
            PreparedStatement ps = null;
            Connection conn = null;
            ResultSet rs;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("SELECT User_Activity.activityID FROM User_Activity, Activity WHERE username = ? and " +
                        "activityName = ? and whatMeasured = ? and units = ? and User_Activity.activityID=Activity.activityID");
                ps.setString(1, username);
                ps.setString(2, curr_activity.getName());
                ps.setString(3, curr_activity.getMeasurements());
                ps.setString(4, curr_activity.getUnits());
                rs = ps.executeQuery();
                if (rs.next()) return rs.getInt("activityID");
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail", "failed");
            } catch (ClassNotFoundException cnfe) {

            } finally {
                try {
                    if (ps != null) ps.close();
                    if (conn != null) conn.close();
                } catch (SQLException sqle) {
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return -1;
        }

        protected Void doInBackground(Void... params) {
            PreparedStatement ps = null;
            Connection conn = null;
            ResultSet workouts;
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                int id = getActivityID();
                ps = conn.prepareStatement("UPDATE Activity SET goal = ? WHERE activityID = ?");
                ps.setInt(1, curr_activity.getGoal());
                ps.setInt(2, id);
                ps.executeUpdate();
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail", "failed");
            } catch (ClassNotFoundException cnfe) {

            } finally {
                try {
                    if (ps != null) ps.close();
                    if (conn != null) conn.close();
                } catch (SQLException sqle) {
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }
    }

}
