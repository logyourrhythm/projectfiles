package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class RegisterGUI extends AppCompatActivity {

    EditText register_username_text, initial_password_text, confirm_password_text;
    Button register_button;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registergui);
        register_username_text = (EditText) findViewById(R.id.register_username_text);
        initial_password_text = (EditText) findViewById(R.id.initial_password_text);
        confirm_password_text = (EditText) findViewById(R.id.confirm_password_text);
        register_button = (Button) findViewById(R.id.register_button);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void checkPassword(View v) {
        //if statement checks if the passwords entered match each other
        if (!(initial_password_text.getText().toString().equals(confirm_password_text.getText().toString()))) {
            AlertDialog.Builder invalidPassword = new AlertDialog.Builder(this);
            invalidPassword.setTitle("Passwords don't match, try again");
            invalidPassword.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    initial_password_text.setText("");
                    confirm_password_text.setText("");
                }
            });
            invalidPassword.show();
        } else { //if passwords match, then go ahead check if username entered is unique
            //check sql database to see if username is taken
            (new UniqueUsernameCheck(register_username_text.getText().toString(),initial_password_text.getText().toString(),this)).execute();
        }
    }

    public void goToUserInfoPage(){
        LoggedUser user = new LoggedUser(register_username_text.getText().toString());
        Intent intent = new Intent(this,UserInfoAfterSignupGUI.class);
        intent.putExtra("LoggedUser",user);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "RegisterGUI Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.tyga.logyourrhythm/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "RegisterGUI Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.tyga.logyourrhythm/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
