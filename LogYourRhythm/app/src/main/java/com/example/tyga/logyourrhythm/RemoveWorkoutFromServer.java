package com.example.tyga.logyourrhythm;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/11/2016.
 */
public class RemoveWorkoutFromServer extends AsyncTask<Void,Void,Void> {
    private LoggedUser user;
    private String workoutName;
    public RemoveWorkoutFromServer(String workoutName, LoggedUser user){
        this.user = user;
        this.workoutName = workoutName;
    }
    @Override
    protected Void doInBackground(Void ... Params){
        PreparedStatement ps = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("DELETE FROM Workout WHERE workoutID=(SELECT workoutID FROM User_Workout" +
                    " WHERE User_Workout.workoutID = Workout.workoutID and User_Workout.username = ?) and workoutName = ?");
            ps.setString(1,user.getUsername());
            ps.setString(2,workoutName);
            ps.executeUpdate();
        } catch (SQLException sqle) {
            Log.d("sqle",sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
        return null;
    }
}
