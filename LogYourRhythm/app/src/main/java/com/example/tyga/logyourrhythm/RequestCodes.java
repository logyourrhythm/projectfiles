package com.example.tyga.logyourrhythm;

/**
 * Created by Tiffany on 4/15/2016.
 * This class holds custom request used for switching between activites
 */
public class RequestCodes {

    //for requests related to my workous
    public static final int creatingWorkoutCode = 1; //request from creating workout to new activity
    public static final int newWorkout = 2; //request from my workouts menu to creating new workout
    public static final int MyWorkoutsGUI = 3; //request from LoggedUserHomeGUI to go to MyWorkoutsGUI

    public static final int MyProgressGUI = 4; //request from LoggedUserHomeGUI to go to MyProgressGUI

    public static final int editProfileCode =5;

    //for requests related to my progress
    public static final int trackingNewProgress = 6; //for request from progress menu to add new progress item
    public static final int viewProgressItem = 7; //for request from progress menu to view a progress item
    public static final int finishedWorkout = 8; //for request from CurrentWorkoutGUI to MyWorkoutsGUI (after a user finishes their current workout)


    public static final int viewMyFriends = 9;
    public static final int SearchFriendGUI = 11;
    public static final int FriendProfileGUI = 10;

    public static final String serverURL ="jdbc:mysql://104.131.26.161:3306/LogYourRhythm?user=root";
}
