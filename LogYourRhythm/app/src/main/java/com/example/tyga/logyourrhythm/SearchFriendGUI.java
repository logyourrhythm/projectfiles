package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.app.AlertDialog;
import android.widget.TextView;
import android.support.v7.widget.SearchView;
import android.content.Context;import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

public class SearchFriendGUI extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private TextView tvOutput;
    private Button addButton;
    private LoggedUser loggedUser;
    private boolean userExists=false;
    private String nameSearching = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final Context context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_friend_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        loggedUser = (LoggedUser)bundle.getSerializable("LoggedUser");

        tvOutput = (TextView) findViewById(R.id.FindUserDisplay);
        addButton  = (Button) findViewById(R.id.addFriendButton);

        addButton.setEnabled(false);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Confirmation");
                alertDialogBuilder
                        .setMessage("Add " + nameSearching + " as friend?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                try{
                                    (new UserFollow(loggedUser.getUsername(),tvOutput.getText().toString())).execute().get();
                                }catch(InterruptedException ie){
                                    ie.printStackTrace();
                                }catch(ExecutionException ee){
                                    ee.printStackTrace();
                                }
                                Log.d("FRIENDS", Integer.toString(loggedUser.getFriends().size()));
                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("LoggedUser",loggedUser);
                                setResult(RESULT_OK,resultIntent);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit (String query){
        tvOutput.setText("Searching " + query);
        nameSearching = query;
        try{
            boolean exists = user_exists(query);
            Log.d("booleans", Boolean.toString(exists) + " " + userExists);
            if(user_exists(query)==true) {
                tvOutput.setText(query);
                addButton.setEnabled(true);
                userExists = false;
            }
            else {
                tvOutput.setText("Can't Find user ID " + query);
            }
        }catch(InterruptedException ie){
            Log.d("tag",ie.getMessage());
        }catch(ExecutionException ee){
            Log.d("tag",ee.getMessage());
        }
        return false;
    }

    public boolean onQueryTextChange (String newText){
        addButton.setEnabled(false);
        tvOutput.setText("Click search button to search...");
       return false;
    }

    private boolean user_exists(String username) throws InterruptedException, ExecutionException{
        (new UsernameQueryToServer(username)).execute().get();
        return userExists;
    }

    class UsernameQueryToServer extends AsyncTask<Void,Void,Void>{
        private String query;
        public UsernameQueryToServer(String query){
            this.query = query;
        }

        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null;
            ResultSet rs;
            Connection conn = null;
            try{
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("SELECT username FROM LoggedUser WHERE" +
                        " username = ?");
                ps.setString(1,query);
                rs = ps.executeQuery();
                if(rs.next()) userExists = true;
//                    while(rs.next()){
//                        String curr_username = rs.getString("username");
//                        boolean alreadyHasFriend = false;
//                        for(LoggedUser l: loggedUser.getFriends()){
//                            if(l.getUsername().equals(curr_username)) alreadyHasFriend = true;
//                        }
//                        if(!alreadyHasFriend && !loggedUser.getUsername().equals(curr_username)){
//                            loggedUser.addFriend(new LoggedUser(curr_username));
//                        }
//                    }
            }catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }
    }

    class UserFollow extends AsyncTask<Void,Void,Void>{
        private String user, userToFollow;
        public UserFollow(String user, String userToFollow){
            this.user = user;
            this.userToFollow = userToFollow;
        }

        protected Void doInBackground(Void ... params){
            PreparedStatement ps = null, ps1 = null;
            Connection conn = null;
            try{
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection(RequestCodes.serverURL);
                ps = conn.prepareStatement("INSERT INTO UserFriends(username1, username2)" +
                        " VALUES(?,?)");
                ps.setString(1,user);
                ps.setString(2,userToFollow);
                ps.execute();
//                    while(rs.next()){
//                        String curr_username = rs.getString("username");
//                        boolean alreadyHasFriend = false;
//                        for(LoggedUser l: loggedUser.getFriends()){
//                            if(l.getUsername().equals(curr_username)) alreadyHasFriend = true;
//                        }
//                        if(!alreadyHasFriend && !loggedUser.getUsername().equals(curr_username)){
//                            loggedUser.addFriend(new LoggedUser(curr_username));
//                        }
//                    }
                ps1 = conn.prepareStatement("INSERT INTO UserFriends(username1, username2)" +
                        " VALUES(?,?)");
                ps1.setString(1,userToFollow);
                ps1.setString(2,user);
                ps1.execute();
                loggedUser.addFriend(new LoggedUser(userToFollow));
                Log.d("FRIENDS", Integer.toString(loggedUser.getFriends().size()));
            }catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
                Log.d("Fail","failed");
            } catch (ClassNotFoundException cnfe) {

            }finally{
                try{
                    if(ps!=null) ps.close();
                    if(ps1!=null) ps1.close();
                    if(conn!=null) conn.close();
                }catch(SQLException sqle){
                    Log.d("sqle", sqle.getMessage());
                }
            }
            return null;
        }
    }
}
