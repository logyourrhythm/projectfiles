package com.example.tyga.logyourrhythm;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/17/2016.
 */
public class UpdateActivityProgressStatusToServer extends AsyncTask<Void,Void,Void> {
    private String workoutName, activityName;
    private LoggedUser user;
    public UpdateActivityProgressStatusToServer(LoggedUser user, String workoutName, String activityName){
        this.user = user;
        this.workoutName = workoutName;
        this.activityName = activityName;
    }

    protected Void doInBackground(Void ... params){
        PreparedStatement ps = null, ps1 = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("UPDATE User_Activity SET\n" +
                    "        User_Activity.progressBoolean =1\n" +
                    "        WHERE username = ? and User_Activity.activityID = (SELECT DISTINCT Activity.activityID FROM Activity, Workout, Workout_Activity\n" +
                    "        WHERE username = ? and Activity.activityName = ? and Activity.activityID =\n" +
                    "        User_Activity.activityID and Workout.workoutName = ? and Workout_Activity.activityID = Activity.activityID)");
            ps.setString(1,user.getUsername());
            ps.setString(2,user.getUsername());
            ps.setString(3,activityName);
            ps.setString(4,workoutName);
            ps.executeUpdate();

        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
        return null;
    }

    protected void onPostExecute(Void result){

    }
}



//UPDATE Workout_Activity SET
//        Workout_Activity.progressBoolean=0
//        WHERE Workout_Activity.workoutID = (SELECT Workout.workoutID FROM Workout, User_Workout WHERE
//        Workout.workoutName = 'workout 1' and Workout.workoutID =
//        User_Workout.workoutID and username = 't')
//
//        UPDATE User_Activity SET
//        User_Activity.progressBoolean =1
//        WHERE username = 't' and User_Activity.activityID = (SELECT DISTINCT Activity.activityID FROM Activity, Workout, Workout_Activity
//        WHERE username = 't' and Activity.activityName = 'timed activity' and Activity.activityID =
//        User_Activity.activityID and Workout.workoutName = 'Legs' and Workout_Activity.activityID = Activity.activityID)