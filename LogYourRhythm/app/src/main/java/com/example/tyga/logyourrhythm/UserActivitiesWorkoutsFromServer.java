package com.example.tyga.logyourrhythm;

import android.app.ActivityManager;
import android.os.AsyncTask;
import android.util.Log;

import java.security.Timestamp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

public class UserActivitiesWorkoutsFromServer extends AsyncTask<Void,Void,Void> {
    //this constructor works for getting a list of workouts or a workout's activities because
    //if you need the list of workouts - give the constructor a null workoutName, the User,
    // and the string "workout" to indicate that you want a list of workouts
    //if you need the list of activities for a specific workout- give the constructor the
    // workout name, the User, and the string "activity" to indicate you want a list of
    // activities for a specific workout

    private User user;
    //    private String
    private Vector<Workout> userWorkouts;
    private Vector<Activity> userActivities;
    private Vector<Activity> progressItems;
    private ProgressManager pM;
    private String defaultSpec;

    //requires an extra parameter- the class that calls this constructor
    // so that in case we need to access any methods of the class that
    // called this class to set any data.
    public UserActivitiesWorkoutsFromServer(User user, String defaultSpec) {
        this.user = user;
        userWorkouts = new Vector<>();
        userActivities = new Vector<>();
        progressItems = new Vector<>();
        this.defaultSpec = defaultSpec;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (pM == null) Log.d("PM NULL", "It's null");
        else {
            Log.d("ProgressItemSize", Integer.toString(pM.getProgresses().size()));
//            if(user instanceof LoggedUser) ((LoggedUser) user).setProgressManager(pM);
        }
    }

    @Override
    protected Void doInBackground(Void... Params) {
        if(user instanceof LoggedUser && ((LoggedUser) user).getUsername().equals("guest")){
            getWorkoutsFromServerNewLogged();
            getNewUserActivitiesFromServer();
        }
        else if (user instanceof LoggedUser && defaultSpec.equals("default")) {
            getWorkoutsFromServerNewLogged();
            getNewUserActivitiesFromServer();
            addDefaultWorkoutsToServer();
        } else if (user instanceof LoggedUser && defaultSpec.equals("")) {
//            ((LoggedUser) user).setProgressManager(pM);
//            pM = ((LoggedUser) user).getProgressManager();
            pM = ((LoggedUser) user).getProgressManager();
            getWorkoutsFromServerLogged();
            getUserActivitiesFromServer();
            getActivityDataForUserActivities();
//            getDefaultActivities();
//            getDefaultWorkouts();
        } else if (user instanceof GuestUser) {
            getWorkoutsFromServerGuest();
        } else {
            Log.d("Nothing happened", "Nothing to see here");
        }
        return null;
    }

    private void getActivityDataForUserActivities() {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet activityData;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            for (Activity a : progressItems) {
                ps = conn.prepareStatement("SELECT activityData, timeRecorded FROM Activity_Data, User_Activity, Activity WHERE\n" +
                        "            User_Activity.progressBoolean = 1 and activityName = ? and\n" +
                        "                    Activity.activityID=Activity_Data.activityID and User_Activity.activityID =\n" +
                        "                    Activity_Data.activityID");
                ps.setString(1, a.getName());
                activityData = ps.executeQuery();
                while (activityData.next()) {
                    a.addData(activityData.getTimestamp("timeRecorded").getTime(), activityData.getInt("activityData"));
                }
            }
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail", "failed");
        } catch (ClassNotFoundException cnfe) {

        } finally {
            try {
                if (ps != null) ps.close();
                if (conn != null) conn.close();
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void getWorkoutsFromServerLogged() {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet workouts;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT Workout.workoutName, Activity.activityName, Activity.units, Activity.whatMeasured, Activity.goal ,progressBoolean " +
                    "FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity " +
                    "WHERE User_Workout.username=? and User_Workout.workoutID = Workout.workoutID " +
                    "and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID = User_Activity.activityID " +
                    "and Workout_Activity.activityID = User_Activity.activityID");
//            SELECT Workout.workoutName, Activity.activityName, Activity.units,
//                    Activity.whatMeasured, progressBoolean
//                    FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity
//                     WHERE
//                     User_Workout.username='t' and User_Workout.workoutID = Workout.workoutID
//                     and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID =
//                    User_Activity.activityID and Workout_Activity.activityID = User_Activity.activityID
            ps.setString(1, ((LoggedUser) user).getUsername());
            workouts = ps.executeQuery();
            Workout curr_workout = null;
            while (workouts.next()) {
                if (curr_workout == null) {
                    curr_workout = new Workout();
                    curr_workout.setWorkoutName(workouts.getString("workoutName"));
                    Activity curr_activity = new Activity(workouts.getString("activityName"),
                            workouts.getString("units"), workouts.getString("whatMeasured"));
                    curr_activity.setGoal(workouts.getInt("goal"));
                    curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                    curr_workout.addActivity(curr_activity);
                    if (workouts.getInt("progressBoolean") != 0) {
                        progressItems.add(curr_activity);
                        pM.addProgressItem(curr_activity, curr_workout);
                    }
                } else {
                    if (!workouts.getString("workoutName").equals(curr_workout.getWorkoutName())) {
                        userWorkouts.add(curr_workout);
                        curr_workout = new Workout();
                        curr_workout.setWorkoutName(workouts.getString("workoutName"));
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_workout.addActivity(curr_activity);
                        if (workouts.getInt("progressBoolean") != 0) {
                            progressItems.add(curr_activity);
                            pM.addProgressItem(curr_activity, curr_workout);
                        }
                    } else {
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_workout.addActivity(curr_activity);
                        if (workouts.getInt("progressBoolean") != 0) {
                            progressItems.add(curr_activity);
                            pM.addProgressItem(curr_activity, curr_workout);
                        }
                    }
                }
            }
            if (curr_workout != null) {
                userWorkouts.add(curr_workout);
                Log.d("Ultimatey ereher", curr_workout.getWorkoutName());
                Log.d("Curr workout", String.valueOf(curr_workout.getNumActivities()));
            }
            WorkoutManager workoutManager = new WorkoutManager();
            for (Workout w : userWorkouts) {
                workoutManager.addWorkout(w);
            }
            ((LoggedUser) user).setWorkoutManager(workoutManager);
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail", "failed");
        } catch (ClassNotFoundException cnfe) {

        } finally {
            try {
                if (ps != null) ps.close();
                if (conn != null) conn.close();
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
            }
        }
    }


    private void getWorkoutsFromServerGuest(){
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet workouts;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT workoutName, activityName, units, whatMeasured, goal, progressBoolean " +
                    "FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity " +
                    "WHERE User_Workout.username='guest' and User_Workout.workoutID = Workout.workoutID " +
                    "and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID = User_Activity.activityID " +
                    "and Workout_Activity.activityID = User_Activity.activityID");
//            SELECT workoutName, activityName, units, whatMeasured, progressBoolean
//                    FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity
//                    WHERE
//                    User_Workout.username='t' and User_Workout.workoutID = Workout.workoutID
//                    and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID =
//                    User_Activity.activityID and Workout_Activity.activityID = User_Activity.activityID

            workouts = ps.executeQuery();
            Workout curr_workout=null;
            while(workouts.next()){
                if(curr_workout == null){
                    curr_workout = new Workout();
                    curr_workout.setWorkoutName(workouts.getString("workoutName"));
                    Activity curr_activity = new Activity(workouts.getString("activityName"),
                            workouts.getString("units"), workouts.getString("whatMeasured"));
                    curr_activity.setGoal(workouts.getInt("goal"));
                    curr_activity.setWorkoutName(workouts.getString("workoutName"));
                    curr_workout.addActivity(curr_activity);
                }else{
                    if(!workouts.getString("workoutName").equals(curr_workout.getWorkoutName())){
                        userWorkouts.add(curr_workout);
                        curr_workout = new Workout();
                        curr_workout.setWorkoutName(workouts.getString("workoutName"));
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(workouts.getString("workoutName"));
                        curr_workout.addActivity(curr_activity);
                    }else {
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(workouts.getString("workoutName"));
                        curr_workout.addActivity(curr_activity);
                    }
                }
            }
            if (curr_workout != null) {
                userWorkouts.add(curr_workout);
                Log.d("Ultimatey ereher", curr_workout.getWorkoutName());
                Log.d("Curr workout", String.valueOf(curr_workout.getNumActivities()));
            }
            for(Workout w: userWorkouts){
                ((GuestUser)user).getWorkoutManager().addWorkout(w);
            }

        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

//    private void getWorkoutActivitiesFromServer(){
//        //get activities and instantiate Activity objects- send them over onPostExecute();
//        PreparedStatement ps = null;
//        Connection conn = null;
//        ResultSet activities = null;
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            conn = DriverManager.getConnection(serverURL);
//            ps = conn.prepareStatement("SELECT activityName, whatMeasured, units " +
//                    "FROM Activity, Workout_Activity, Workout, User_Workout WHERE" +
//                    "workoutName=? and username=? and User_Workout.workoutID = Workout.workoutID" +
//                    "and Workout_Activity.workoutID = Workout.workoutID ");
//            ps.setString(1,workoutName);
//            ps.setString(2,user.getUsername());
//            activities = ps.executeQuery();
//            while(activities.next()){
//                String name, units, measurements;
//                name = activities.getString("activityName");
//                units = activities.getString("units");
//                measurements = activities.getString("whatMeasured");
//                Activity workoutActivity = new Activity(name,units,measurements);
//                workoutActivities.add(workoutActivity);
//            }
//            activities.close();
//        } catch (SQLException sqle) {
//            Log.d("sqle", sqle.getMessage());
//            Log.d("Fail","failed");
//        } catch (ClassNotFoundException cnfe) {
//
//        }finally{
//            try{
//                if(ps!=null) ps.close();
//                if(conn!=null) conn.close();
//            }catch(SQLException sqle){
//                Log.d("sqle", sqle.getMessage());
//            }
//        }
//    }

    //this can be used to autofill things- on a EditText
    private void getUserActivitiesFromServer() {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet activities;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT activityName, units, whatMeasured, goal, workoutName, progressBoolean " +
                    "FROM User_Activity, Activity, Workout, Workout_Activity WHERE Workout.workoutID = Workout_Activity.workoutID " +
                    "and User_Activity.username = ? and User_Activity.activityID = Activity.activityID " +
                    "and Activity.activityID = Workout_Activity.activityID");
            if (user instanceof GuestUser) ps.setString(1, ((GuestUser) user).getUsername());
            else ps.setString(1, ((LoggedUser) user).getUsername());
            activities = ps.executeQuery();
            while (activities.next()) {
                Activity curr_activity = new Activity(activities.getString("activityName")
                        , activities.getString("units"), activities.getString("whatMeasured"));
                curr_activity.setGoal(activities.getInt("goal"));
                curr_activity.setWorkoutName(activities.getString("workoutName"));
                userActivities.add(curr_activity);
//                curr_activity.setProgressIsTracked(activities.getInt("progressBoolean")!=0);

            }
            activities.close();
            activityManager aM = new activityManager((LoggedUser) user);
            Log.d("ACTIVITIES SIZE", String.valueOf(userActivities.size()));
            for (Activity a : userActivities) {
                aM.addActivity(a);
            }
            ((LoggedUser) user).setActivityManager(aM);
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail", "failed");
        } catch (ClassNotFoundException cnfe) {

        } finally {
            try {
                if (ps != null) ps.close();
                if (conn != null) conn.close();
            } catch (SQLException sqle) {
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void addDefaultWorkoutsToServer(){
        PreparedStatement ps = null,ps1=null,ps2=null, ps3=null,ps4=null;
        ResultSet mostRecentActivities;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("INSERT INTO Workout(whenCreated, workoutName) VALUES(?,?)");
            ps1 = conn.prepareStatement("INSERT INTO User_Workout(username, workoutID) VALUES(?," +
                    "(SELECT workoutID FROM Workout ORDER BY workoutID DESC LIMIT 1))");
            ps2 = conn.prepareStatement("INSERT INTO Activity(activityName,units,whatMeasured) VALUES(?,?,?)");
            ps3 = conn.prepareStatement("INSERT INTO User_Activity(username,activityID) VALUES(?," +
                    "(SELECT activityID FROM Activity ORDER BY activityID DESC LIMIT 1))");
            ps4 = conn.prepareStatement("INSERT INTO Workout_Activity(workoutID,activityID) VALUES(" +
                    "(SELECT workoutID FROM User_Workout where username = ? ORDER BY workoutID DESC LIMIT 1)," +
                    "(SELECT activityID FROM User_Activity WHERE username = ? ORDER BY activityID DESC LIMIT 1))");
            for(Workout w: userWorkouts){
                ps.setTimestamp(1,new java.sql.Timestamp(new java.util.Date().getTime()));
                ps.setString(2, w.getWorkoutName());
                ps.execute();
                ps1.setString(1, ((LoggedUser) user).getUsername());
                ps1.execute();
                for(Activity a: w.getActivitiesList()){
                    ps2.setString(1,a.getName());
                    ps2.setString(2,a.getUnits());
                    ps2.setString(3,a.getMeasurements());
                    ps2.execute();
                    ps3.setString(1, ((LoggedUser) user).getUsername());
                    ps3.execute();
                    ps4.setString(1, ((LoggedUser) user).getUsername());
                    ps4.setString(2,((LoggedUser) user).getUsername());
                    ps4.execute();
                }
            }
        } catch (SQLException sqle) {
            Log.d("sqleGGOOO", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void getDefaultWorkouts(){
        PreparedStatement ps = null;
        ResultSet workouts;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT Workout.workoutName, Activity.activityName, Activity.units, Activity.whatMeasured, Activity.goal ,progressBoolean " +
                    "FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity " +
                    "WHERE User_Workout.username='guest' and User_Workout.workoutID = Workout.workoutID " +
                    "and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID = User_Activity.activityID " +
                    "and Workout_Activity.activityID = User_Activity.activityID");
            workouts = ps.executeQuery();
            Workout curr_workout=null;
            while(workouts.next()){
                if(curr_workout == null){
                    curr_workout = new Workout();
                    curr_workout.setWorkoutName(workouts.getString("workoutName"));
                    Activity curr_activity = new Activity(workouts.getString("activityName"),
                            workouts.getString("units"), workouts.getString("whatMeasured"));
                    curr_activity.setGoal(workouts.getInt("goal"));
                    curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                    curr_workout.addActivity(curr_activity);
                    if(workouts.getInt("progressBoolean")!=0){
                        progressItems.add(curr_activity);
                        pM.addProgressItem(curr_activity,curr_workout);
                    }
                }else{
                    if(!workouts.getString("workoutName").equals(curr_workout.getWorkoutName())){
                        userWorkouts.add(curr_workout);
                        curr_workout = new Workout();
                        curr_workout.setWorkoutName(workouts.getString("workoutName"));
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_workout.addActivity(curr_activity);
                        if(workouts.getInt("progressBoolean")!=0){
                            progressItems.add(curr_activity);
                            pM.addProgressItem(curr_activity,curr_workout);
                        }
                    }else {
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_workout.addActivity(curr_activity);
                        if(workouts.getInt("progressBoolean")!=0){
                            progressItems.add(curr_activity);
                            pM.addProgressItem(curr_activity,curr_workout);
                        }
                    }
                }
            }
            if(curr_workout!=null) {
                userWorkouts.add(curr_workout);
                Log.d("Ultimatey ereher", curr_workout.getWorkoutName());
                Log.d("Curr workout", String.valueOf(curr_workout.getNumActivities()));
            }
            WorkoutManager workoutManager=((LoggedUser)user).getWorkoutManager();
            if(workoutManager!=null){
                Log.d("WM NOT NULL", "WM NOT NULL");
            }
            for(Workout w: userWorkouts){
                workoutManager.addWorkout(w);
            }
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void getDefaultActivities(){
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet activities;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT activityName, units, whatMeasured, goal, workoutName, progressBoolean " +
                    "FROM User_Activity, Activity, Workout, Workout_Activity WHERE Workout.workoutID = Workout_Activity.workoutID " +
                    "and User_Activity.username = 'guest' and User_Activity.activityID = Activity.activityID " +
                    "and Activity.activityID = Workout_Activity.activityID");
            activities = ps.executeQuery();
            while(activities.next()){
                Activity curr_activity = new Activity(activities.getString("activityName")
                        ,activities.getString("units"),activities.getString("whatMeasured"));
                curr_activity.setGoal(activities.getInt("goal"));
                curr_activity.setWorkoutName(activities.getString("workoutName"));
                userActivities.add(curr_activity);
//                curr_activity.setProgressIsTracked(activities.getInt("progressBoolean")!=0);

            }
            activities.close();
            activityManager aM =((LoggedUser)user).getActivityManager();

            for(Activity a: userActivities) {
                aM.addActivity(a);
            }
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void getWorkoutsFromServerNewLogged(){
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet workouts;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT workoutName, activityName, units, whatMeasured, goal, progressBoolean " +
                    "FROM Workout, User_Workout, Workout_Activity, Activity, User_Activity " +
                    "WHERE User_Workout.username='guest' and User_Workout.workoutID = Workout.workoutID " +
                    "and Workout.workoutID = Workout_Activity.workoutID and Activity.activityID = User_Activity.activityID " +
                    "and Workout_Activity.activityID = User_Activity.activityID");
            workouts = ps.executeQuery();
            Workout curr_workout=null;
            while(workouts.next()){
                if(curr_workout == null){
                    curr_workout = new Workout();
                    curr_workout.setWorkoutName(workouts.getString("workoutName"));
                    Activity curr_activity = new Activity(workouts.getString("activityName"),
                            workouts.getString("units"), workouts.getString("whatMeasured"));
                    curr_activity.setGoal(workouts.getInt("goal"));
                    curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                    curr_workout.addActivity(curr_activity);
                }else{
                    if(!workouts.getString("workoutName").equals(curr_workout.getWorkoutName())){
                        userWorkouts.add(curr_workout);
                        curr_workout = new Workout();
                        curr_workout.setWorkoutName(workouts.getString("workoutName"));
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_workout.addActivity(curr_activity);
                    }else {
                        Activity curr_activity = new Activity(workouts.getString("activityName"),
                                workouts.getString("units"), workouts.getString("whatMeasured"));
                        curr_activity.setGoal(workouts.getInt("goal"));
                        curr_activity.setWorkoutName(curr_workout.getWorkoutName());
                        curr_workout.addActivity(curr_activity);
                    }
                }
            }
            if(curr_workout!=null) {
                userWorkouts.add(curr_workout);
                Log.d("Ultimatey ereher", curr_workout.getWorkoutName());
                Log.d("Curr workout", String.valueOf(curr_workout.getNumActivities()));
            }
            WorkoutManager workoutManager=new WorkoutManager();
            for(Workout w: userWorkouts){
                workoutManager.addWorkout(w);
            }
            ((LoggedUser) user).setWorkoutManager(workoutManager);
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

    private void getNewUserActivitiesFromServer(){
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet activities;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT activityName, units, whatMeasured, goal, workoutName FROM User_Activity," +
                    "Activity, Workout, Workout_Activity WHERE Workout.workoutID = Workout_Activity.workoutID and " +
                    "User_Activity.username = 'guest' and User_Activity.activityID = Activity.activityID and Activity.activityID =" +
                    "Workout_Activity.activityID");
            activities = ps.executeQuery();
            while(activities.next()){
                Activity curr_activity = new Activity(activities.getString("activityName")
                        ,activities.getString("units"),activities.getString("whatMeasured"));
                curr_activity.setGoal(activities.getInt("goal"));
                curr_activity.setWorkoutName(activities.getString("workoutName"));
                userActivities.add(curr_activity);
            }
            activities.close();
            activityManager aM = new activityManager((LoggedUser)user);
            Log.d("ACTIVITIES SIZE", String.valueOf(userActivities.size()));
            for(Activity a: userActivities) aM.addActivity(a);
            ((LoggedUser) user).setActivityManager(aM);

        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
    }

}

//SELECT activityName, units, whatMeasured, workoutName, User_Activity.progressBoolean FROM User_Activity,
//        Activity, Workout, Workout_Activity WHERE Workout.workoutID = Workout_Activity.workoutID and
//        User_Activity.username = 't' and User_Activity.activityID = Activity.activityID and Activity.activityID =
//        Workout_Activity.activityID