package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Vector;

public class UserFriendListGUI extends AppCompatActivity {

    //DATA MEMBERS
    private ListView l;
    private Button addFriendButton;
    private LoggedUser loggedUser;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> array;
    private Vector<LoggedUser> friends;
    private TextView numFriendsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_friend_list_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        addFriendButton = (Button) findViewById(R.id.newFriendButton);
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(UserFriendListGUI.this, SearchFriendGUI.class);
                intent.putExtra("LoggedUser", loggedUser);
                startActivityForResult(intent, RequestCodes.SearchFriendGUI);
            }
        });
        // get user data
        Bundle bundle = getIntent().getExtras();
        loggedUser = (LoggedUser) bundle.getSerializable("LoggedUser");
        friends = loggedUser.getFriends();

        if (friends.size() == 0) {
            Toast.makeText(this, "No friends!", Toast.LENGTH_SHORT).show();
        }

        numFriendsTextView = (TextView) findViewById(R.id.numFriendsTextView);
        numFriendsTextView.setText("You have " + friends.size() + " friends!");

        l = (ListView) findViewById(R.id.listView);
        array = new ArrayList<String>();
        // put friends' name into array
        for(int i = 0; i < friends.size(); ++i)
        {
            array.add(friends.elementAt(i).getUsername());
        }

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        //set the listview adapter
        l.setAdapter(arrayAdapter);
        l.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view,
                                            int position, long id) {
                        Object o = l.getItemAtPosition(position);
                        String username = o.toString();
                        //TODO open friend profile here
                        //The friend we're going to is friends.elementAt(position)

                        //open profile page
                        Intent intent = new Intent(UserFriendListGUI.this,FriendProfGUI.class);
                        intent.putExtra("LoggedUser", loggedUser);
                        intent.putExtra("FriendProfile", username);
                        startActivityForResult(intent, RequestCodes.FriendProfileGUI);
                    }
                }
        );


        setSupportActionBar(toolbar);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.FriendProfileGUI && resultCode == RESULT_OK && null!=data) {
            Bundle bundle = data.getExtras();
            loggedUser = (LoggedUser)bundle.getSerializable("LoggedUser");
//            numFriendsTextView.setText("You have " + friends.size() + " friends!");
//            friends = loggedUser.getFriends();
//            updateFriendsView();
        }else if(requestCode == RequestCodes.SearchFriendGUI && resultCode == RESULT_OK && null != data){
            Bundle bundle = data.getExtras();
            loggedUser = (LoggedUser)bundle.getSerializable("LoggedUser");
            friends = loggedUser.getFriends();
            numFriendsTextView.setText("You have " + friends.size() + " friends!");
            updateFriendsView();
        }
    }


    private void updateFriendsView(){
        array = new ArrayList<String>();
        // put friends' name into array
        for(int i = 0; i < friends.size(); ++i)
        {
            Log.d("FRIENDS", friends.elementAt(i).getUsername());
            array.add(friends.elementAt(i).getUsername());
        }

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        //set the listview adapter
        l.setAdapter(arrayAdapter);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(UserFriendListGUI.this, LoggedUserHomeGUI.class);
        i.putExtra("LoggedUser", loggedUser);
        setResult(RESULT_OK, i);
        super.onBackPressed();
        finish();
    }

}