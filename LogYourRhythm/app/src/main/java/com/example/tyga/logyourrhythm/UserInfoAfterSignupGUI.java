package com.example.tyga.logyourrhythm;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.concurrent.ExecutionException;

//page that user sees after they sign up so they can fill out their name, goal, height, weight
public class UserInfoAfterSignupGUI extends AppCompatActivity {
    private EditText firstNameEditText, lastNameEditText, heightEditText, weightEditText, goalEditText;
    private Button submitButton;
    private int PICK_IMAGE = 1;
    private ImageView profilePictureImageView;
    private byte[] profilePictureBlob;
    private boolean profilePictureChosen=false;
    private LoggedUser loggedUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfoaftersignupgui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        heightEditText = (EditText) findViewById(R.id.heightEditText);
        weightEditText = (EditText) findViewById(R.id.weightEditText);
        goalEditText = (EditText) findViewById(R.id.goalEditText);
        submitButton = (Button) findViewById(R.id.submitButton);
        profilePictureImageView = (ImageView) findViewById(R.id.no_profile_picture);

        Bundle user = getIntent().getExtras();
        if(user!=null){
            loggedUser=(LoggedUser)user.getSerializable("LoggedUser");
        }
    }

    public void decodeUri(Uri uri) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to
            final int REQUIRED_SIZE = 1024;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(imageSource, null, o2);
            profilePictureImageView.setImageBitmap(bitmap);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            profilePictureBlob = bos.toByteArray();

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
    }

    public void chooseProfilePicture(View view){
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Show only images, no videos or anything else
//
//        // Always show the chooser (if there are multiple options available)
//        startActivityForResult(intent, PICK_IMAGE);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select Picture"), PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null!=data) {
            decodeUri(data.getData());
            profilePictureChosen = true;
//            String[] filePathColumn = { MediaStore.Images.Media.DATA };
//            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            String picturePath = cursor.getString(columnIndex);
//            cursor.close();
//            profilePictureImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//            profilePictureBlob = (Blob) profilePictureImageView;
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
//                profilePictureImageView = (ImageView) findViewById(R.id.no_profile_picture);
//                profilePictureImageView.setImageBitmap(bitmap);
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
//                profilePictureBlob = bos.toByteArray();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

    public void saveUserInfo(View view)throws InterruptedException, ExecutionException{
        if(!profilePictureChosen){
            profilePictureImageView.setDrawingCacheEnabled(true);
            profilePictureImageView.buildDrawingCache();
            Bitmap bm = profilePictureImageView.getDrawingCache();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            profilePictureBlob = stream.toByteArray();
        }
        int height=0,weight=0;
        if(!heightEditText.getText().toString().isEmpty()) height = Integer.parseInt(heightEditText.getText().toString());
        if(!weightEditText.getText().toString().isEmpty()) weight = Integer.parseInt(weightEditText.getText().toString());
        (new UserInfoToServer(profilePictureBlob,firstNameEditText.getText().toString(),
                lastNameEditText.getText().toString(),goalEditText.getText().toString(),height
                ,weight,loggedUser)).execute();

        (new UserActivitiesWorkoutsFromServer(loggedUser,"default")).execute().get();
        Intent intent = new Intent(UserInfoAfterSignupGUI.this,LoggedUserHomeGUI.class);
        intent.putExtra("LoggedUser", loggedUser);
        startActivity(intent);
        finish();
    }


}
