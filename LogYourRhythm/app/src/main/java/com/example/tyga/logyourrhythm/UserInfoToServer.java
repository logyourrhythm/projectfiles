package com.example.tyga.logyourrhythm;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by Tyga on 4/13/2016.
 */
public class UserInfoToServer extends AsyncTask<Void,Void,Void> {
    private String firstName,lastName, goal;
    private int height,weight;
    private byte[] profilePicture;
    private LoggedUser user;
    public UserInfoToServer(byte[] profilePicture, String firstName, String lastName,
                            String goal, int height, int weight, LoggedUser user){
        this.profilePicture = profilePicture;
        this.firstName=firstName;
        this.lastName = lastName;
        this.goal= goal;
        this.height=height;
        this.weight=weight;
        this.user = user;
    }

    @Override
    protected Void doInBackground(Void ... params){ //update LoggedUser and put image into LoggedUserImage
        PreparedStatement ps = null, ps1= null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("UPDATE LoggedUser SET" +
                    " firstName=?, lastName=?, goal = ?," +
                    "height=?, weight=? WHERE username=? ");
            ps.setString(1, firstName);
            ps.setString(2,lastName);
            ps.setString(3, goal);
            ps.setInt(4, height);
            ps.setInt(5, weight);
            ps.setString(6, user.getUsername());
            ps.executeUpdate();

            ps1 = conn.prepareStatement("INSERT INTO LoggedUserImage(username,image,timeUploaded)" +
                    " VALUES(?,?,?)");
            ps1.setString(1,user.getUsername());
            ps1.setBinaryStream(2, new ByteArrayInputStream(profilePicture),profilePicture.length); //attempt to get length of null array
            ps1.setTimestamp(3,new Timestamp(System.currentTimeMillis()));
            ps1.execute();
        } catch (SQLException sqle) {
            Log.d("sqle", sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(ps1!=null) ps1.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        //now load preloaded workouts from the guest into the
    }
}
