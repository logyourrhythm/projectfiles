package com.example.tyga.logyourrhythm;

public class UserProfile {

    int userType;
    //0 for guest, 1 for registered

    private User user;

    public UserProfile()
    {
        userType = 0;
        user = new GuestUser();
    }

    public UserProfile(User u)
    {
        userType = 1;
        user = u;
    }

    int getUserType() {return userType;}

    User getUser() {return user;}

}
