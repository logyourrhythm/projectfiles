package com.example.tyga.logyourrhythm;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ViewWorkoutGUI extends AppCompatActivity {
    //DATA MEMBERS
    private Workout workout;
    private ListView l;
    private ArrayList<Activity> workoutActivities;

    //COSNTRUCTOR
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_workout_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Get Workout
        Bundle bundle = getIntent().getExtras();
        workout = (Workout) bundle.get("Workout");

        //Set the title of the screen to the workout name
        TextView workoutNameTextView = (TextView) findViewById(R.id.titleTextView);
        workoutNameTextView.setText(workout.getWorkoutName());

        //Get and display the list of activities in the workout
        l = (ListView) findViewById(R.id.userOptionsList);
        Vector<Activity> activitiesVector =  workout.getActivitiesList();
        workoutActivities = new ArrayList<Activity>(activitiesVector);
        ActivityListAdapter adapter = new ActivityListAdapter(this, R.layout.custom_list_view, workoutActivities);
        l.setAdapter(adapter);

        //Floating action button is a cancel button that just takes the user back a screen
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
