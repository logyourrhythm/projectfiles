package com.example.tyga.logyourrhythm;

import com.example.tyga.logyourrhythm.Activity;

import java.io.Serializable;
import java.util.Vector;

/**
 * this class represents a single workout which is comprised of an ordered list of activities
 *
 */

public class Workout implements Serializable {
    public static final long SerialVersionUID = 1L;

    //DATA MEMBERS
    private Vector<Activity> activitiesList;
    private String username; //probably not needed
    private User user;
    private int numActivities;
    private String workoutName;

    //CONSTRUCTOR
    public Workout() {
        numActivities=0;
        activitiesList = new Vector<Activity>(); //create an empty vector
        numActivities = 0;
    }

    //METHODS

    //set methods
    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }
    public void setActivitiesList(Vector<Activity> newActivityList) { this.activitiesList = newActivityList;}

    //get methods, unsure if they will be used
    public int getNumActivities() {
        return numActivities;
    }
    public Vector<Activity> getActivitiesList() {
        return activitiesList;
    }
    public String getWorkoutName() {
        return workoutName;
    }

    //this adds an activity to the end of the activity list
    public void addActivity(Activity activityToAdd) {
        activitiesList.addElement(activityToAdd);
        numActivities += 1;
    }

    //this allows an activity to be added at any position in the list of activities
    public void insertActivity(Activity activityToInsert, int position) {
        activitiesList.add(position, activityToInsert);
        numActivities += 1;
    }

    //this will remove an instance of an activity from the list
    //note: might need to rewrite this so it can removed based on an index position
    public void removeActivity(Activity activityToRemove) {
        if (activitiesList.contains(activityToRemove)) {
            activitiesList.removeElement(activityToRemove);
            numActivities -= 1;
        }
    }

}
