package com.example.tyga.logyourrhythm;

import android.util.Log;

import com.example.tyga.logyourrhythm.Workout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

/**
 * The workout manager will keep track of all of a user's workouts as well as the preloaded workouts
 *  that all users will have access to
 */

public class WorkoutManager implements Serializable{
    //DATA MEMBERS
    public static final long serialVersionUID = 1;
    private Vector<Workout> userWorkouts;
    private HashMap<Workout, Integer> workoutFrequency;
    //private User user;

    //CONSTRUCTOR
    public WorkoutManager(){
        userWorkouts = new Vector<Workout>();
        workoutFrequency = new HashMap<Workout,Integer>();
    }

    //METHODS
    public void addWorkout(Workout workoutToAdd) {
        userWorkouts.addElement(workoutToAdd);
        workoutFrequency.put(workoutToAdd, 0); //0 frequency
    }

    public Vector<Workout> getUserWorkouts() {
        return userWorkouts;
    }


    //If a user chooses to remove a workout from their workouts, this will delete it
    //This does not allow the user to remove workouts that are preloaded
    public boolean removeWorkout(String workoutToRemoveName) {
        for (Workout w : userWorkouts) {
            if (w.getWorkoutName().equals(workoutToRemoveName)) {
                userWorkouts.remove(w);
                workoutFrequency.remove(w);
                return true; //workout removal was successful
            }
        }
        return false; //the remove was not successful
    }

    //starting a workout allows the user to enter information about each workout's activity
    //starting a workout increases its frequency of being used
    public void startWorkout(Workout workoutToStart) {

    }

    //find the workout in the hashtable that has the highest frequency
    // based each time the user starts a workout
    public Workout getMostFrequentWorkout() {
        if (workoutFrequency.size() != 0) {
            Workout mostFrequentWorkout = null;
            int mostFrequentCount = 0;
            for (Workout w : workoutFrequency.keySet() ) {
                int currentFrequency = workoutFrequency.get(w);
                if (currentFrequency > mostFrequentCount) {
                    mostFrequentCount = currentFrequency;
                    mostFrequentWorkout = w;
                }
            }
            return mostFrequentWorkout;
        }
        return null;
    }

    public void increaseWorkoutFrequency(Workout w) {
        if (workoutFrequency.containsKey(w)) {
            workoutFrequency.put(w, workoutFrequency.get(w) + 1);
        }
    }

    public Workout getWorkoutByName(String workoutName) {
        for (Workout w : userWorkouts) {
            if (w.getWorkoutName().equals(workoutName)) {
                return w;
            }
        }
        return null;
    }

    public void updateWorkoutActivity(Activity a) {
        String workoutName = a.getWorkoutName();
        Workout w = getWorkoutByName(workoutName);
        int workoutPos = userWorkouts.indexOf(w);
        Vector<Activity> activitiesList = w.getActivitiesList();
        int targetPos = 0;
        for (Activity activity : activitiesList) {
            if (activity.getName().equals(a.getName())) {
                activitiesList.set(targetPos, a);
                //swap old activity for updated activity
                break;
            }
            targetPos+=1;
        }
        w.setActivitiesList(activitiesList);
        userWorkouts.set(workoutPos, w);
    }


}
