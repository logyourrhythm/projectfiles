package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;


public class activityGUI extends AppCompatActivity {

    private LoggedUser user;
    private activityManager activityManager;
    private Vector<String> activityNames;
    private String activityName, activityMeasurements, activityUnits, workoutName;
    private GoogleApiClient client;
    private AutoCompleteTextView nameTextField;
    private Spinner measurementsSpinner, unitsSpinner;
    ArrayAdapter<CharSequence> measurementsAdapter, unitsAdapter;
    ArrayAdapter<String> nameTextFieldAdapater;
    private Button addButton;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_gui);
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        user = (LoggedUser) bundle.getSerializable("LoggedUser");
        workoutName = (String) bundle.getSerializable("WorkoutName");

        activityManager = user.getActivityManager();
        activityNames = activityManager.getActivityNames();
        activityNames.add("swerveypoo");

        measurementsSpinner = (Spinner)findViewById(R.id.measurementsSpinner);
        unitsSpinner = (Spinner)findViewById(R.id.unitsSpinner);
        measurementsAdapter = ArrayAdapter.createFromResource(this, R.array.measurementsArray, android.R.layout.simple_spinner_item);
        measurementsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        measurementsSpinner.setAdapter(measurementsAdapter);
        addButton = (Button) findViewById(R.id.addButton);
        nameTextField = (AutoCompleteTextView) findViewById(R.id.activityNameTextField);
        nameTextFieldAdapater = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, activityNames);
        nameTextField.setAdapter(nameTextFieldAdapater);

        addActions();

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void addActions() {
        //changes the unitsSpinner according to which measurement is picked (eg Distance pulls up the miles and kilometers units)
        measurementsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(),parent.getSelectedItem().toString() + " Selected", Toast.LENGTH_LONG).show();
                String measurement = (String) parent.getItemAtPosition(position);
                if(measurement.equals("Distance")) {
                    unitsAdapter = ArrayAdapter.createFromResource(context, R.array.distanceUnits, android.R.layout.simple_spinner_item);
                    unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    unitsSpinner.setAdapter(unitsAdapter);
                } else if(measurement.equals("Weight")) {
                    unitsAdapter = ArrayAdapter.createFromResource(context, R.array.weightUnits, android.R.layout.simple_spinner_item);
                    unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    unitsSpinner.setAdapter(unitsAdapter);
                } else if(measurement.equals("Time")) {
                    unitsAdapter = ArrayAdapter.createFromResource(context, R.array.timeUnits, android.R.layout.simple_spinner_item);
                    unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    unitsSpinner.setAdapter(unitsAdapter);
                } else if(measurement.equals("Repetitions")) {
                    unitsAdapter = ArrayAdapter.createFromResource(context, R.array.repetitionsUnits, android.R.layout.simple_spinner_item);
                    unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    unitsSpinner.setAdapter(unitsAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() { //creating a new activity, adding it to workout, activitymanager, and server
            public void onClick(View v) {
                activityName = nameTextField.getText().toString();
                activityMeasurements = measurementsSpinner.getSelectedItem().toString();
                activityUnits = unitsSpinner.getSelectedItem().toString();

                //check if name is filled
                if (activityName.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activityGUI.this);
                    builder.setMessage("Enter an Activity Name")
                            .setTitle("Error");
                    // Add the buttons
                    builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    // Create the AlertDialog
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return;
                }

                Activity activityToSend = new Activity(activityName,activityUnits,activityMeasurements);
                activityToSend.setWorkoutName(workoutName);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("Activity", activityToSend);
                resultIntent.putExtra("LoggedUser",user);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "activityGUI Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.tyga.logyourrhythm/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "activityGUI Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.tyga.logyourrhythm/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
