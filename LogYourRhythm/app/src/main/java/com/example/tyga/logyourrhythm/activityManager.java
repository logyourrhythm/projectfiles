package com.example.tyga.logyourrhythm;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by Andrew Nguyen on 4/8/2016.
 */

public class activityManager implements Serializable{
    public static final long serialVersionUID=1;
    private LoggedUser user;
    private Vector<Activity> availableActivities; //set of all activities (preloaded and added by user)
    private Vector<String> activityNames; //contains the names of all activities (for the autocomplete)

    public activityManager(LoggedUser user) {
        this.user = user;
        availableActivities = new Vector<Activity>();
        activityNames = new Vector<String>();
    }

    //adds an activity to activityManager and activityName to set of names
    public void addActivity(Activity activity) {
        availableActivities.add(activity);
        activityNames.add(activity.getName());
    }

    public boolean removeActivity(Activity activity) {
        for(Activity a : availableActivities) {
            if(a.getName().equals(activity.getName())) {
                activityNames.remove(a.getName());
                availableActivities.remove(a);
                return true;
            }
        }
        return false;
    }

    public Vector<Activity> getActivities() {
//        (new UserActivitiesWorkoutsFromServer(user,"")).execute();
        return availableActivities;
    }

    public void updateActivity(Activity activity) {
        /*Iterator it = availableActivities.iterator();
        Activity activityToRemove = null;
        while(it.hasNext()) {
            String availableActivity = activity.getName();
            String availableActivityWorkout = activity.getWorkoutName();
            if( (availableActivity.equals(activity.getName())) && (availableActivityWorkout.equals(activity.getWorkoutName())) ) {
                activityToRemove = (Activity)it.next();
                break;
            }
        }
        removeActivity(activityToRemove);
        addActivity(activity);*/
        System.out.println(activity.getName());
        int targetPos = 0;
        for(Activity a : availableActivities) {
            if( (a.getName().equals(activity.getName())) && (a.getWorkoutName().equals(activity.getWorkoutName())) ) {
                System.out.println(a.getName());
                availableActivities.set(targetPos, activity);
                break;
            }
            targetPos += 1;
        }
    }

    public Vector<String> getActivityNames() {
        return activityNames;
    }

    public Activity getActivityByName(String activityName) {
        for (Activity a : availableActivities) {
            System.out.println(a.getName());
            if (a.getName().equals(activityName)) {
                return a;
            }
        }
        return null;
    }

}


