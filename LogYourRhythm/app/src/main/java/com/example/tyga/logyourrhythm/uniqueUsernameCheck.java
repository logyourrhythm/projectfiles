package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/9/2016.
 */
public class UniqueUsernameCheck extends AsyncTask<Void, Void, Void> {
    private String usernameToCheck,passwordToHash;
    private boolean isUnique;
    private static final String serverURL = "jdbc:mysql://104.131.26.161:3306/LogYourRhythm?user=root";
    private RegisterGUI registerGUI;
    public UniqueUsernameCheck(String username, String passwordToHash, RegisterGUI registerGUI){
        usernameToCheck=username;
        this.passwordToHash=passwordToHash;
        this.registerGUI = registerGUI;
        isUnique = true;
    }

    @Override
    protected Void doInBackground(Void... credentials){
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(serverURL);
            ps = conn.prepareStatement("SELECT username FROM LoggedUser where username=?");
            ps.setString(1,usernameToCheck);
            rs = ps.executeQuery();
            if(rs.next()) {
                isUnique = false;
            }
            rs.close();
        } catch (SQLException sqle) {
            Log.d("sqle",sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        if(isUnique){
            (new NewUserToServer(usernameToCheck,passwordToHash, registerGUI)).execute();
        }else{
            AlertDialog.Builder invalidCredentialsDialog = new AlertDialog.Builder(registerGUI); //Read Update
            invalidCredentialsDialog.setTitle("Error");
            invalidCredentialsDialog.setMessage("Username is taken. Try again.");
            invalidCredentialsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            invalidCredentialsDialog.show();
        }
    }
}
