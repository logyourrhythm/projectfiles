package com.example.tyga.logyourrhythm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Tyga on 4/9/2016.
 */
public class ValidCredentialsCheck extends AsyncTask<Void,Void,Void> {
    private String usernameToCheck, passwordToCheck;
    private boolean usernameExists, goodPassword;
    private LoginGUI loginGUI;
    public ValidCredentialsCheck(String username, String password, LoginGUI loginGUI){
        usernameToCheck = username;
        passwordToCheck = password;
        this.loginGUI = loginGUI;
    }
    protected Void doInBackground(Void ... credentials) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(RequestCodes.serverURL);
            ps = conn.prepareStatement("SELECT username, password FROM LoggedUser where username=?");
            ps.setString(1,usernameToCheck);
            rs = ps.executeQuery();
            while (rs.next()) {
                String checkAgainstUsername = rs.getString("username");
                String checkAgainstPassword = rs.getString("password");
                Log.d("username", checkAgainstUsername);
                Log.d("pw",checkAgainstPassword);
                //include BCrypt logic here to validate password later
                //insert temporary pw checker here
                if(checkAgainstUsername.equals(usernameToCheck) && BCrypt.checkpw(passwordToCheck,checkAgainstPassword)){
                    usernameExists = true;
                    goodPassword = true;
                }else{
                    usernameExists=false;
                    goodPassword=false;
                }

            }
            rs.close();
        } catch (SQLException sqle) {
            Log.d("sqle",sqle.getMessage());
            Log.d("Fail","failed");
        } catch (ClassNotFoundException cnfe) {

        }finally{
            try{
                if(ps!=null) ps.close();
                if(conn!=null) conn.close();
            }catch(SQLException sqle){
                Log.d("sqle", sqle.getMessage());
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result){ //takes action depending on results of SQL query
        if(usernameExists && goodPassword){
            loginGUI.goToHomePage();
        }
        else{
            AlertDialog.Builder invalidCredentialsDialog = new AlertDialog.Builder(loginGUI); //Read Update
            invalidCredentialsDialog.setTitle("Error");
            invalidCredentialsDialog.setMessage("Please enter a valid username or password.");
            invalidCredentialsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            invalidCredentialsDialog.show();
        }
    }
}
